#!/bin/sh
# TRACKINGTOOLS
export TRACKINGTOOLS_ON=1
export TRACKINGTOOLS="$(pwd)/.."
export CPATH="${TRACKINGTOOLS}/include/core:${TRACKINGTOOLS}/include/detector:${TRACKINGTOOLS}/include/stream"
