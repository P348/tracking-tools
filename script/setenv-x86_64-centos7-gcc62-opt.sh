#!/bin/sh
# GCC
source /cvmfs/sft.cern.ch/lcg/contrib/gcc/6.2/x86_64-centos7-gcc62-opt/setup.sh
# ROOT
source /cvmfs/sft.cern.ch/lcg/releases/LCG_87/ROOT/6.08.02/x86_64-centos7-gcc62-opt/bin/thisroot.sh
# BOOST
source /cvmfs/sft.cern.ch/lcg/releases/LCG_87/Boost/1.62.0/x86_64-centos7-gcc62-opt/Boost-env.sh
# EIGEN
source /cvmfs/sft.cern.ch/lcg/releases/LCG_87/eigen/3.2.9/x86_64-centos7-gcc62-opt/eigen-env.sh
# GENFIT
export GENFIT=/afs/cern.ch/work/h/hsieber/public/opt/genfit-x86_64-centos7-gcc62
# CMAKE
export PATH=/cvmfs/sft.cern.ch/lcg/releases/LCG_87/CMake/3.5.2/x86_64-centos7-gcc62-opt/bin:${PATH}
# GSL
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/cvmfs/sft.cern.ch/lcg/releases/LCG_87/GSL/2.1/x86_64-centos7-gcc62-opt/lib
