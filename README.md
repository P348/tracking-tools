# Generic tracking library

This library is a generic library for tracking using a given set of track candidates. It relies on the use of different tracking algorithms and is built is such a way that it only cares about the inputs $`(X,Y,Z,\sigma)`$.

## Building the library
To build the library, following dependences are needed and should be pin-pointed to in the user-defined environment:
* [ROOT] - A modular scientific software toolkit.
* [Eigen] - A C++ template library for linear algebra: matrices, vectors, numerical solvers, and related algorithms.
* [GENFIT] - A generic toolkit for track reconstruction for experiments in particle and nuclear physics.
* [BOOST] - Free-peer reviewed portable C++ source libraries.

Once having correctly set-up the environment, the library can be built by (on LXPLUS):
```sh
$ mkdir build && cd build/
$ cmake -DGENFIT_INCLUDE_DIR=/path/to/GenFit/include -DGENFIT_LIBRARY=/path/to/GenFit/lib/libgenfit.so -DCMAKE_C_COMPILER=`which gcc` -DCMAKE_CXX_COMPILER=`which g++` ..
$ make -j4
$ make install
```
The corresponding library is installed in the `lib/` directory and the necessary files for `cmake` function `FIND_PACKAGE` are installed in the `share/cmake/` directory.

The environment for the library to be used in other programs can be set by entering:
```sh
$ cd script
$ source makeEnv.sh
```

## The examples
Examples of the use of this library are located in the `examples/` directory.

## The tracking algorithms
The base tracking algorithm to be used is with [GENFIT]. There exist a base class, called `TrackingUser`, and already implemented abstract classes such as `TrackingGenFit`, `TrackingAnalytical` or `TrackingCircle` with each a different tracking algorithm.


[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)


   [EIGEN]: <http://eigen.tuxfamily.org>
   [ROOT]: <https://root.cern.ch>
   [GENFIT]: <http://genfit.sourceforge.net/Main.html>
   [BOOST]: <https://www.boost.org>


