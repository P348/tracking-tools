#ifndef PCALinearReg_hh
#define PCALinearReg_hh

// STD Library
#include <iostream>
#include <vector>

// ROOT
#include "TRandom.h"

// Eigen
#include <Eigen/Dense>

class PCALinearReg{

  public:

    PCALinearReg(std::vector<Eigen::RowVector3d>&, TRandom* = 0, double Sigma = 0.);     // l-value
    PCALinearReg(std::vector<Eigen::RowVector3d>&&, TRandom* = 0, double Sigma = 0.);    // r-value
    ~PCALinearReg();

    void AddSmearing();

    void PreProcessData();
    void CalculateUnit();
    void Fit();

    // Get methods
    std::vector<Eigen::RowVector3d> GetData();
    Eigen::MatrixXd GetDataMatrix();
    Eigen::Vector3d GetClusterSeed();
    Eigen::Vector3d GetUnitVector();
    double GetChisqFit();

    void PrintFitInfo();


  private:

    std::vector<Eigen::RowVector3d> fData;
    TRandom* fRnd;
    double  fSigma;
    Eigen::MatrixXd fMat;
    Eigen::Vector3d fUnit;

    double fChisq;
    std::vector<Eigen::RowVector2d> fDataExp;
    std::vector<Eigen::RowVector2d> fDataObs;

};

#endif  // PCALinearReg_hh
