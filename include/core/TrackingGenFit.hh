#ifndef TrackingGenFit_hh
#define TrackingGenFit_hh

#include "TrackingUser.hh"
#include "TrackingManager.hh"
#include "MagneticFieldMap.hh"
#include "IOStreamBuffer.hh"

// STD Library
#include <utility>
#include <vector>
#include <string>

// ROOT
#include <TMatrixDSym.h>
#include <TVector3.h>
#include <TGeoManager.h>
#include <TClonesArray.h>
#include <TClass.h>

// GenFit
#include <Track.h>
#include <AbsKalmanFitter.h>
#include <mySpacepointDetectorHit.h>
#include <MeasurementFactory.h>

typedef std::map<std::string, utils::MagneticFieldMap> kFieldMap;

namespace Tracking{

  struct Track{
    double kMomentum;
    double kChisquare;

    std::vector<TVector3> kTrueHits;
    std::vector<TVector3> kRecoHits;

    genfit::Track* gfTrack;

    Track() : kMomentum(9999.), kChisquare(-1.), gfTrack(nullptr)
    {
      // ...
    }

    ~Track() {
      //if (gfTrack) delete gfTrack;
    }

    void SetMom(double Momentum) {kMomentum = Momentum;}
    void SetChisquare(double Chisquare) {kChisquare = Chisquare;}
    void SetTrueHits(std::vector<TVector3> TrueHits) {kTrueHits = TrueHits;}
    void SetRecoHits() {
      for (unsigned int i = 0; i < gfTrack->getNumPoints(); i++) 
        kRecoHits.push_back(gfTrack->getFittedState(i).getPos()*10.);
    }
    void SetGenFitTrack(genfit::Track* track) {gfTrack = track;}

    TVector3 ExtrapolateToPos(TVector3& pos) {

      // extrapolate
      genfit::StateOnPlane state = gfTrack->getFittedState(0);
      double trackLength = gfTrack->getCardinalRep()->extrapolateToPoint(state, pos);

      TVector3 result = state.getPos();

      // return to previous state
      gfTrack->getCardinalRep()->extrapolateBy(state, -trackLength);
      return result;
    }
  };

}

class TrackingGenFit : public TrackingUser
{

  public:

    TrackingGenFit();
    ~TrackingGenFit();

    enum kFitterType{
      Kalman, KalmanRefTrack, DAF
    };

    void FitTrackCandidates();
    std::pair<double, double> FitTrack(std::vector<std::pair<TVector3, double> >&);

    void Clean();

    // set methods
    void SetPDG(double);
    void SetNominalEnergy(double);
    void SetInitDir(TVector3);
    void SetGDMLManager(TGeoManager*);
    void SetMagnets(std::vector<Magnet::Generic*>);
    void SetMagneticFieldMaps(kFieldMap&);
    void SetMBPLField(bool);
    void SetNIter(const int);

    void PrintResults(std::vector<TVector3>&);

    // get methods
    bool GetMBPLField();
    std::vector<Tracking::Track> GetReconstructedTracks();

    // init methods
    void InitGeometry();
    void InitMBPLField();
    void InitNonUniformField();
    void InitMatrices(const unsigned int, const double);
    void InitGeomAndFields();
    void InitMeasurementsFactory();
    void InitFitter(kFitterType&&);

  private:

    int fPDG;
    int fNIter;
    double fNominalEnergy;
    TVector3 fInitDir;
    bool fMBPLField;
    std::vector<Magnet::Generic*> fMagnets;
    kFieldMap fFieldMaps;

    TMatrixDSym fHitCov;
    TMatrixDSym fHitCovA;

    // GenFit
    TGeoManager* fGeoManager;
    
    genfit::AbsKalmanFitter* fFitter;
    
    // Streambuf to redirect GenFit output
    IOStreamBuffer* _rdbufDbg;
    IOStreamBuffer* _rdbufErr;
    IOStreamBuffer* _rdbufOut;

    int fMyDetId;
    genfit::MeasurementFactory<genfit::AbsMeasurement> fFactory;
    TClonesArray fMyDetectorHitArray;

    int fNTracks;
    TClonesArray fTrackCandArray;
    std::vector<Tracking::Track> fTrackCand;

};

#endif // TrackingGenFit_hh
