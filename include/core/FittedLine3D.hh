#ifndef FittedLine3D_hh
#define FittedLine3D_hh

// STD Library
#include <iostream>
#include <vector>

// Eigen
#include <Eigen/Dense>

class FittedLine3D{

  public:

    FittedLine3D(Eigen::Vector3d&, Eigen::Vector3d&);       // l-value
    FittedLine3D(Eigen::Vector3d&&, Eigen::Vector3d&&);     // r-value
    ~FittedLine3D();

    // set methods (l- and r-value)
    void SetUnitVector(Eigen::Vector3d&);
    void SetUnitVector(Eigen::Vector3d&&);
    void SetSeed(Eigen::Vector3d&);
    void SetSeed(Eigen::Vector3d&&);

    // get methods
    Eigen::Vector3d GetPosition(double);
    Eigen::Vector3d GetUnitVector();
    Eigen::Vector3d GetSeed();
    Eigen::Vector3d GetExtrapolation(double);

    void PrintLine();

    // POCA
    Eigen::Vector3d GetPOCA(FittedLine3D&);

  private:

    Eigen::Vector3d fUnit;
    Eigen::Vector3d fSeed;

};

#endif  // FittedLine3D
