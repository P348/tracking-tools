#ifndef TrackingUser_hh
#define TrackingUser_hh

#include "TrackerGeneric.hh"
#include "MagnetGeneric.hh"

// STD Library
#include <utility>
#include <string>
#include <vector>
#include <map>

// ROOT
#include "TVector3.h"
#include "TRandom.h"

class TrackingUser{

  public:

    TrackingUser();
    virtual ~TrackingUser();

    virtual void FitTrackCandidates() {;}
    virtual std::pair<double, double> FitTrack(std::vector<std::pair<TVector3, double> >&) = 0;

    virtual void Clean() = 0;

    void SetRnd(TRandom*);
    void SetSmearing(double);
    void SetMagnet(Magnet::Generic*);
    void SetTrackCandidates(std::map<std::string, Tracker::Generic>);
    std::vector<std::vector<std::pair<TVector3,double> > >  GetCombinations(const std::vector<std::vector<std::pair<TVector3,double> > >&);

    // TODO: see below
    std::vector<std::pair<double, double> > GetFittedTracks();
    std::vector<std::vector<TVector3> > GetRecoTracks();
    std::vector<std::vector<TVector3> > GetTrueTracks();

    void ClearInputs();

  protected:

    TRandom* fRnd;
    double fSmearing;
    Magnet::Generic* fMagnet;
    std::map<std::string, Tracker::Generic> fCandidates;
    
    // TODO: create a track structure that contains both the input as
    // reconstructed position, as well as chi-square scoring and momentum
    std::vector<std::pair<double, double> > fFittedTracks;
    std::vector<std::vector<TVector3> > fRecoTracks;
    std::vector<std::vector<TVector3> > fTrueTracks;


};

#endif // TrackingUser_hh
