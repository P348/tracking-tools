#ifndef TrackingAnalytical_hh
#define TrackingAnalytical_hh

#include "TrackingUser.hh"

// STD Library
#include <utility>
#include <vector>

// ROOT
#include "TVector3.h"

class TrackingAnalytical : public TrackingUser
{

  public:

    TrackingAnalytical();
    ~TrackingAnalytical();

    void FitTrackCandidates();
    std::pair<double, double> FitTrack(std::vector<std::pair<TVector3, double> >&);

    void Clean() {;}

    void SetNumberOfPlanes(unsigned int);
    void SetBackPropagate(bool);

    double SimpleTracking3(
        const TVector3, const TVector3, const TVector3,
        double, double, double);
    double SimpleTracking4(
        const TVector3, const TVector3, const TVector3, const TVector3,
        double, double);

  private:

    unsigned int fNPlanes;
    bool fBackPropagate;

};

#endif // TrackingAnalytical_hh
