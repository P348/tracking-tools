#ifndef TrackingCircle_hh
#define TrackingCircle_hh

#include "TrackingUser.hh"

// STD Library
#include <utility>
#include <vector>

// ROOT
#include "TVector3.h"

class TrackingCircle : public TrackingUser
{

  public:

    TrackingCircle();
    ~TrackingCircle();

    void FitTrackCandidates();
    std::pair<double, double> FitTrack(std::vector<std::pair<TVector3, double> >&);

    void Clean() {;}

  private:

    // ...

};

#endif // TrackingCircle_hh
