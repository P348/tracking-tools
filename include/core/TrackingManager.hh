#ifndef TrackingManager_hh
#define TrackingManager_hh

#include "TrackingUser.hh"
#include "DetectorFactory.hh"

// STD Library
#include <vector>

// ROOT
#include <TVector3.h>

class TrackingManager{

  public:
    TrackingManager();
    ~TrackingManager();

    static TrackingManager* GetInstance();

    void RegisterFactory(DetectorFactory*);
    DetectorFactory* GetFactory();

    void SetTrackingAlgorithm(TrackingUser*);
    TrackingUser* GetTrackingAlgorithm();

    void SetInputs(std::vector<std::pair<TVector3,double> >);
    void ClearInputs();

    void Clean();

    void FindTrackCandidates();
    void FitTrackCandidates();

    void SetFittedTracks(std::vector<std::pair<double, double> >);
    std::vector<std::pair<double, double> > GetFittedTracks();
    std::vector<std::vector<TVector3> > GetRecoTracks();
    std::vector<std::vector<TVector3> > GetTrueTracks();

  private:

    static TrackingManager* fMgrInstance;

    DetectorFactory* fDetectorFactory;
    TrackingUser* fTrackingUser;

    std::vector<std::pair<TVector3, double> > fPoints;

    std::vector<std::pair<double, double> > fFittedTracks;
    std::vector<std::vector<TVector3> > fRecoTracks;
    std::vector<std::vector<TVector3> > fTrueTracks;
};

#endif // TrackingManager_hh
