/* Copyright 2008-2010, Technische Universitaet Muenchen,
   Authors: Christian Hoeppner & Sebastian Neubert & Johannes Rauch

   This file is part of GENFIT.

   GENFIT is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published
   by the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   GENFIT is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with GENFIT.  If not, see <http://www.gnu.org/licenses/>.
*/
/** @addtogroup genfit
 * @{
 */

#ifndef genfit_ConstField3Box_hh
#define genfit_ConstField3Box_hh

#include "AbsBField.h"

namespace genfit {

class ConstField3Box : public AbsBField {
 public:
  //! define the constant field in this ctor
 ConstField3Box(double b11, double b12, double b13, 
     double b21, double b22, double b23,
     double b31, double b32, double b33,
  double xmin1, double xmax1, double ymin1, double ymax1, double zmin1, double zmax1,
  double xmin2, double xmax2, double ymin2, double ymax2, double zmin2, double zmax2,
  double xmin3, double xmax3, double ymin3, double ymax3, double zmin3, double zmax3
  )
    : field1_(b11, b12, b13),field2_(b21, b22, b23),field3_(b31, b32, b33)
    {
      _xmin1 = xmin1;
      _xmax1 = xmax1;
      _ymin1 = ymin1;
      _ymax1 = ymax1;
      _zmin1 = zmin1;
      _zmax1 = zmax1;

      _xmin2 = xmin2;
      _xmax2 = xmax2;
      _ymin2 = ymin2;
      _ymax2 = ymax2;
      _zmin2 = zmin2;
      _zmax2 = zmax2;

      _xmin3 = xmin3;
      _xmax3 = xmax3;
      _ymin3 = ymin3;
      _ymax3 = ymax3;
      _zmin3 = zmin3;
      _zmax3 = zmax3;
    }

  ConstField3Box(const TVector3& field1,const TVector3& field2,const TVector3& field3)
    : field1_(field1),field2_(field2),field3_(field3)
  { ; }

  //! return value at position
  TVector3 get(const TVector3& pos) const;
  void get(const double& posX, const double& posY, const double& posZ, double& Bx, double& By, double& Bz) const;

 private:
  TVector3 field1_;
  TVector3 field2_;
  TVector3 field3_;
  double _xmin1;
  double _xmax1;
  double _ymin1;
  double _ymax1;
  double _zmin1;
  double _zmax1;
  double _xmin2;
  double _xmax2;
  double _ymin2;
  double _ymax2;
  double _zmin2;
  double _zmax2;
  double _xmin3;
  double _xmax3;
  double _ymin3;
  double _ymax3;
  double _zmin3;
  double _zmax3;
};

} /* End of namespace genfit */
/** @} */

#endif // genfit_ConstField3Box_hh
