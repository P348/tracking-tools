#ifndef FieldCuboid_hh
#define FieldCuboid_hh 1

/*! \file FieldCuboid.hh
    \author Henri Hugo Sieber <henri.hugo.sieber@cern.ch>
*/

// ROOT
#include <Math/Vector3D.h>
#include <Math/Point3D.h>
#include <Math/EulerAngles.h>
#include <Math/Translation3D.h>
#include <Math/RotationZYX.h>
#include <Math/Rotation3D.h>
#include <Math/Transform3D.h>

// STD Library
#include <iostream>
#include <stdlib.h>

class FieldCuboid {

  /*! \class FieldCuboid
      \brief Definition of a cuboid.

      This class defines the geometry of a cuboid to encapsulate e.g. discrete
      magnetic fields defined on a cuboidal grid. The cuboid should always be
      defined with respect to the coordinate system O with its center at (0,0,0).
      Then it is placed in space according to the 3D transformation matrix.
  */


  public:

    FieldCuboid();

    ~FieldCuboid();

    void SetVertex(unsigned int, double, double, double);
    void SetVertex(unsigned int, double*);
    void SetVertex(unsigned int, ROOT::Math::XYZVector&);

    //void SetTransformation(const ROOT::Math::EulerAngles&, const ROOT::Math::Translation3D&); 
    void SetTransformation(const ROOT::Math::RotationZYX&, const ROOT::Math::Translation3D&); 
    ROOT::Math::Transform3D GetTransformation() const; 

    bool IsWithinBoundary(ROOT::Math::XYZVector&) const;
    ROOT::Math::XYZVector GetCenter(bool) const;

  private:

    double fVertices[8][3];

    ROOT::Math::Transform3D fTMatrix;
};

inline FieldCuboid::FieldCuboid() { ; }
inline FieldCuboid::~FieldCuboid(){ ; }

inline void FieldCuboid::SetVertex( unsigned int i
                                  , double x
                                  , double y
                                  , double z )
{
  /*! \fn void SetVertex(unsigned int i, double x, double y, double z)
      \brief Assign the coordinates (x,y,z) to the vertex i

      The vertices are assumed to be defined as follow. For the reference coordinate system O
      centered at the origin (0,0,0), vertices 0...3 are on the lower half plane (y < 0) and 
      4...7 in the upper half plane (y > 0).

      \param i The vertex
      \param x The x coordinate
      \param y The y coordinate
      \param z The z coordinate
  */

  fVertices[i][0] = x; fVertices[i][1] = y; fVertices[i][2] = z;

}

inline void FieldCuboid::SetVertex( unsigned int i
                                  , double* v )
{
  /*! \fn void SetVertex(unsigned int i, double* v)
      \brief Assign the coordinates v to the vertex i
  */

  fVertices[i][0] = v[0]; fVertices[i][1] = v[1]; fVertices[i][2] = v[2];

}

inline void FieldCuboid::SetVertex( unsigned int i
                                  , ROOT::Math::XYZVector& v )
{
  /*! \fn void SetVertex(unsigned int i, ROOT::Math::XYZVector& v)
      \brief Assign the coordinates v to the vertex i
  */

  fVertices[i][0] = v.X(); fVertices[i][1] = v.Y(); fVertices[i][2] = v.Z();

}

inline void FieldCuboid::SetTransformation( /*const ROOT::Math::EulerAngles& rotation*/
                                            const ROOT::Math::RotationZYX& rotation
                                          , const ROOT::Math::Translation3D& translation )
{
  /*! \fn void SetTransformation(const ROOT::Math::EulerAngles& rotation, cont ROOT::Math::Translation3D& translation)
      \brief Assign the transformation matrix.
  */

  ROOT::Math::Transform3D temp(ROOT::Math::Rotation3D(rotation), translation);
  fTMatrix = temp;
#if 0
  ROOT::Math::Rotation3D rot = fTMatrix.Rotation();
  ROOT::Math::XYZVector v,u,w;
  rot.GetComponents(v,u,w);
  std::cout << "Rotation: \n"; 
  std::cout << v.X() << "," << v.Y() << "," << v.Z() << std::endl;
  std::cout << u.X() << "," << u.Y() << "," << u.Z() << std::endl;
  std::cout << w.X() << "," << w.Y() << "," << w.Z() << std::endl;
  std::cout << "Translation: \n";
  fTMatrix.GetTranslation(v);
  std::cout << v.X() << "," << v.Y() << "," << v.Z() << std::endl;
#endif
}

inline ROOT::Math::Transform3D FieldCuboid::GetTransformation() const
{
  return fTMatrix;
}

inline bool FieldCuboid::IsWithinBoundary(ROOT::Math::XYZVector& point) const
{
  /*! \fn bool IsWithinBoundary(ROOT::Math::XYZVector& point)
      \brief Check whether a point is within the cuboid volume

      With respect to the (0,0,0) point, the geometry-defining vector are
      assumed to be: p1 ~ (+,-,-), p2 ~ (-,-,-), p4 ~ (+,-,+), p5 ~ (+,+,-)
  */

  // define geometry-defining vector
  ROOT::Math::XYZPoint p1; p1.SetCoordinates(fVertices[0]); 
  ROOT::Math::XYZPoint p2; p2.SetCoordinates(fVertices[1]); 
  ROOT::Math::XYZPoint p4; p4.SetCoordinates(fVertices[3]); 
  ROOT::Math::XYZPoint p5; p5.SetCoordinates(fVertices[4]); 

  // transform them
  /* FIXME: remove pointer to transformation matrix. 
  if( !fTMatrix ) {
    std::cerr << " Error in <FieldCuboid::IsWithinBoundary>: transformation matrix undefined.\n";
    exit(EXIT_FAILURE);
  }
  */

  p1 = fTMatrix*p1;
  p2 = fTMatrix*p2;
  p4 = fTMatrix*p4;
  p5 = fTMatrix*p5;

  // compute the dot products
  ROOT::Math::XYZVector pp1(p1.X(),p1.Y(),p1.Z());
  ROOT::Math::XYZVector pp2(p2.X(),p2.Y(),p2.Z());
  ROOT::Math::XYZVector pp4(p4.X(),p4.Y(),p4.Z());
  ROOT::Math::XYZVector pp5(p5.X(),p5.Y(),p5.Z());
  ROOT::Math::XYZVector pp = point-pp1;
  double dot1 = pp.Dot(pp2-pp1);
  double dot2 = pp.Dot(pp4-pp1);
  double dot3 = pp.Dot(pp5-pp1);

  bool check = (0 < dot1 && dot1 < (pp2-pp1).Mag2()) ? true : false;
  if( !check ) return false;
  check = (0 < dot2 && dot2 < (pp4-pp1).Mag2()) ? true : false;
  if( !check ) return false;
  check = (0 < dot3 && dot3 < (pp5-pp1).Mag2()) ? true : false;

  return check;
}

inline ROOT::Math::XYZVector FieldCuboid::GetCenter(bool transformedSystem = false) const
{
  /*! \fn ROOT::Math::XYZVector GetCenter(bool transformedSystem)
      \brief Compute the center of the cuboid.

      \param transformedSystem Give the position relative to the transformation.
  */

  double center[3] = {0.0,0.0,0.0};
  for( unsigned int i = 0; i < 8; i++ ) {
    center[0] += fVertices[i][0];
    center[1] += fVertices[i][1];
    center[2] += fVertices[i][2];
  }

  center[0] /= 8.;
  center[1] /= 8.;
  center[2] /= 8.;

  ROOT::Math::XYZVector v;
  fTMatrix.GetTranslation(v);

  ROOT::Math::XYZVector result; result.SetCoordinates(center);
  if( transformedSystem ) result += v;

  return result;
}

#endif // FieldCuboid_hh
