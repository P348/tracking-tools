#ifndef MagnetMBPL_hh
#define MagnetMBPL_hh

#include "MagnetGeneric.hh"

namespace Magnet{

  struct MBPLDimensions{

    MBPLDimensions(double Xmin, double Xmax, double Ymin, double Ymax, double Zmin, double Zmax)
    {
      fXmin = Xmin;
      fXmax = Xmax;
      fYmin = Ymin;
      fYmax = Ymax;
      fZmin = Zmin;
      fZmax = Zmax;
    }

    MBPLDimensions() {}
    ~MBPLDimensions() {}

    double fXmin, fXmax;
    double fYmin, fYmax;
    double fZmin, fZmax;

    double GetXmin() {return fXmin;}
    double GetXmax() {return fXmax;}
    double GetYmin() {return fYmin;}
    double GetYmax() {return fYmax;}
    double GetZmin() {return fZmin;}
    double GetZmax() {return fZmax;}

  };

  class MBPL : public Generic {
  
    public:
  
      MBPL(std::string);
      ~MBPL();

      void SetMBPLDimensions(Magnet::MBPLDimensions);
      MBPLDimensions GetMBPLDimensions();

    private:

      MBPLDimensions fDimensions;

  };

} // end namespace Magnet

#endif // MagnetMBPL_hh
