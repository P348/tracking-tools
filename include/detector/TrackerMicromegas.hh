#ifndef TrackerMicromegas_hh
#define TrackerMicromegas_hh

#include "TrackerGeneric.hh"

namespace Tracker{

  class Micromegas : public Generic {

    public:

      Micromegas(std::string);
      ~Micromegas();

    private:

      // ...

  };

} // end namespace Tracker

#endif // TrackerMicromegas_hh
