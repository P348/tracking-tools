#ifndef DetectorFactory_hh
#define DetectorFactory_hh

#include "TrackerGeneric.hh"
#include "MagnetGeneric.hh"

// STD Library
#include <string>
#include <map>

// ROOT
#include "TList.h"
#include "TGeoManager.h"

class DetectorFactory{

  public:
    DetectorFactory();
    ~DetectorFactory();

    static DetectorFactory* GetInstance();

    void SetFactoryName(std::string);

    // Geometry
    void SetGDMLFile(std::string, int);
    TGeoManager* GetGDMLGeoManager();

    // Detectors
    void RegisterNewTracker(std::string, Tracker::Generic);
    Tracker::Generic GetRegisteredTracker(std::string);
    void AddHitToRegisteredTracker(std::string, std::pair<TVector3, double>);
    void PrintListOfRegisteredTrackers(bool);
    std::map<std::string, Tracker::Generic> GetListOfRegisteredTrackers();
    int GetNumberOfRegisteredTrackers();
    void ClearRegisteredTrackersHits();
    void ClearListOfRegisteredTrackers();

    // Magnets
    void RegisterNewMagnet(std::string, Magnet::Generic*);
    void RegisterListOfMagnets(std::vector<Magnet::Generic*>&);
    Magnet::Generic* GetRegisteredMagnet(std::string);
    void PrintListOfRegisteredMagnets();
    std::map<std::string, Magnet::Generic*> GetListOfRegisteredMagnets();
    int GetNumberOfRegisteredMagnets();
    void ClearListOfRegisteredMagnets();

    void Initialise();

  private:

    static DetectorFactory* fFactoryInstance;

    std::string fFactoryName;

    std::string fGDMLFileName;
    TGeoManager* fGDMLGeoManager;

    std::map<std::string, Tracker::Generic> fFactoryTrackers;
    std::map<std::string, Magnet::Generic*> fFactoryMagnets;

};

#endif // DetectorFactory_hh
