#ifndef MagneticFieldMap_hh
#define MagneticFieldMap_hh 1

// STD Library 
#include <string>
#include <utility>
#include <vector>
#include <iostream>

// ROOT
#include <TVector3.h>

// User
#include "FieldCuboid.hh"

namespace utils{

  typedef std::pair<TVector3, TVector3> kGridPoint;

  struct GridAxis
  {

    double pMin;
    double pMax;
    double pNPoints;
    double pStep;

    GridAxis()
    {
      // ...
    }

    GridAxis(double Min, double Max, double NPoints, double Step) :
      pMin(Min), pMax(Max), pNPoints(NPoints), pStep(Step)
    {
      // ...
    }

    void CalculateStep()
    {
      pStep = (pMax-pMin)/(pNPoints-1);
    }

    void ShiftAxis(double shift)
    {
      pMin += shift;
      pMax += shift;
    }

    void GetInfo()
    {
      std::cout << pMin << " " << pMax << " " 
        << pStep << " " << pNPoints << std::endl;
    }

  };

  struct FieldGrid
  {

    std::vector<utils::kGridPoint> pGrid;
    utils::GridAxis pXAxis;
    utils::GridAxis pYAxis;
    utils::GridAxis pZAxis;

    FieldGrid()
    { 
      // ...
    }

  };

  class MagneticFieldMap
  {

    public:
      MagneticFieldMap();
      MagneticFieldMap(std::string);
      ~MagneticFieldMap();

      void SetVerboseLevel(unsigned int);

      void SetGlobalShift(const TVector3&);
      void SetGlobalRotation(const TVector3&);
      void SetGlobalScaling(double);
      void InitFieldGrid(std::string);
      void PrintGrid();

      bool CheckRange(const TVector3&) const;

      // get methods
      std::string GetName() const {return fName;}
      FieldGrid GetGrid() const {return fGrid;}
      TVector3 GetGridPointIndex(const TVector3&) const;
      TVector3 GetGridPointIndex(const TVector3&&) const ;
      utils::kGridPoint GetGridPointValue(const TVector3&) const;
      utils::kGridPoint GetGridPointValue(const TVector3&&) const;
      utils::kGridPoint GetClosestNeighbour(const TVector3&) const;

      TVector3 InterpolateFieldValue(const TVector3&) const;

      // overloading
      MagneticFieldMap operator=(const MagneticFieldMap& aMap);

    private:

      std::string fName;

      unsigned int fVerboseLvl;
      TVector3 fGlobalShift;
      TVector3 fGlobalRot;
      double fGlobalScaling;

      FieldGrid fGrid;
      FieldCuboid fCuboid;

  };

} // end namespace utils

#endif // MagneticFieldMap_hh
