#ifndef MagnetGeneric_hh
#define MagnetGeneric_hh

// STD Library
#include <string>

// ROOT
#include <TVector3.h>

namespace Magnet{

  class Generic{

    public:

      Generic(std::string);
      virtual ~Generic();

      void SetMagnetName(std::string);
      void SetFieldMag(double);
      void SetFieldDir(TVector3);

      std::string GetMagnetName();
      double GetFieldMag();
      TVector3 GetFieldDir();

    protected:

      std::string fMagnetName;
      double fFieldMag;
      TVector3 fFieldDir;

    private:

      // ...
  };

} // end namespace Magnet

#endif // MagnetGeneric_hh
