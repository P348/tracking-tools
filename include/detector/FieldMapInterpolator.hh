#ifndef FIELD_MAP_INTERPOLATOR_HH
#define FIELD_MAP_INTERPOLATOR_HH 1

/**
 * @file FieldMapInterpolator.hh
 * @brief Header file for the FieldMapInterpolator class, which provides methods
 * to parse, interpolate, and transform 3D magnetic field data.
 *
 * The FieldMapInterpolator class reads 2D field maps from a file, extends them
 * into 3D using symmetry, and interpolates the magnetic field at any given position.
 * It supports affine transformations for translating and rotating the field grid.
 *
 * @version 1.0
 * @date 2024-11-26
 *
 * @author Henri Hugo Sieber <henri.hugo.sieber@cern.ch>
 */

// STD Library
#include <iostream>
#include <fstream>
#include <map>

// Eigen
#include <Eigen/Dense>

using Vector3 = Eigen::Vector3d;
using Matrix3 = Eigen::Matrix3d;

/**
 * @class FieldMapInterpolator
 * @brief Class to interpolate a 3D magnetic field map based on 2D octant data.
 *
 * This class parses a 2D field map from a file and extends it into 3D space,
 * leveraging symmetry properties to handle multiple octants. It supports trilinear
 * interpolation and transformations to map query points into the first octant.
 */
class FieldMapInterpolator
{
  /**
   * @struct CuboidBounds
   * @brief Stores the dimensions, resolution, and spacings of the 3D field grid.
   */
  struct CuboidBounds{

    double m_xmin; //!< Minimum x-coordinate of the grid.
    double m_xmax; //!< Maximum x-coordinate of the grid.
    double m_ymin; //!< Minimum y-coordinate of the grid.
    double m_ymax; //!< Maximum y-coordinate of the grid.
    double m_zmin; //!< Minimum z-coordinate of the grid.
    double m_zmax; //!< Maximum z-coordinate of the grid.

    std::uint32_t m_nx; //!< Number of grid points in x.
    std::uint32_t m_ny; //!< Number of grid points in y.
    std::uint32_t m_nz; //!< Number of grid points in z.

    double m_dx; //!< Spacing between grid points in x.
    double m_dy; //!< Spacing between grid points in y.
    double m_dz; //!< Spacing between grid points in z.
  };

  public:

  /**
   * @brief Default constructor.
   */
  FieldMapInterpolator() = default;

  /**
   * @brief Constructs the FieldMapInterpolator and parses the input file.
   * @param fileName Path to the input file containing the 2D field map.
   * @param L Half the length of the z-dimension (-L to L).
   * @param nz Number of grid points in the z-dimension.
   * @param scaling Global scaling factor of the magnetic field.
   */
  FieldMapInterpolator( const std::string& fileName, const double& L, std::uint32_t nz, const double& scaling ) {
    ParseFieldFile( fileName, L, nz, scaling );
  }

  /**
   * @brief Default destructor.
   */
  ~FieldMapInterpolator() = default;

  /**
   * @brief Sets an affine transformation to translate and rotate the field map.
   * @param translation Vector representing the translation.
   * @param rotation Rotation matrix.
   */
  void SetAffineTransformation( const Vector3& translation, const Matrix3& rotation ) {
    m_translation = translation;
    m_rotation    = rotation;
  }

  /**
   * @brief Retrieves the interpolated field vector at a given position.
   * @param position 3D position in global coordinates.
   * @return Interpolated magnetic field vector at the specified position.
   */
  const Vector3 GetField( const Vector3& position ) const;

  /**
   * @brief Retrieves the bounds of the field grid.
   * @return A CuboidBounds struct containing grid bounds and spacings.
   */
  const CuboidBounds GetBounds() const { return m_bounds; }

  private:

  /**
   * @brief Parses the 2D field map file and extends it into 3D space.
   * @param fileName Path to the input file containing the 2D field map.
   * @param L Half the length of the z-dimension (-L to L).
   * @param nz Number of grid points in the z-dimension.
   */
  void ParseFieldFile( const std::string& fileName, const double& L, std::uint32_t nz, const double& scaling );

  CuboidBounds m_bounds;                 //!< Grid bounds and resolution.
  std::vector<Vector3> m_fieldData;      //!< Flattened vector storing the 3D field data.

  Vector3 m_translation = Vector3::Zero(); //!< Translation vector for affine transformation.
  Matrix3 m_rotation    = Matrix3::Identity(); //!< Rotation matrix for affine transformation.

  /**
   * @brief Computes the 1D index in the flattened field data for a given grid point.
   * @param ix Grid index along the x-dimension.
   * @param iy Grid index along the y-dimension.
   * @param iz Grid index along the z-dimension.
   * @return Linearized 1D index corresponding to the 3D grid point.
   */
  std::uint32_t Index( std::uint32_t ix, std::uint32_t iy, std::uint32_t iz ) const {
    return ix+m_bounds.m_nx*(iy+m_bounds.m_ny*iz);
  }

  /**
   * @brief Reflects a position vector into the first octant using symmetry.
   * @param position Position vector to be reflected.
   * @param flipAxis Array indicating which axes were flipped.
   */
  void FirstOctantSymmetry( Vector3& position, std::array< bool, 3 >& flipAxis ) const
  {
    for( std::uint32_t iAxis = 0; iAxis <3; iAxis++ )
    {
      flipAxis.at( iAxis ) = position( iAxis ) < 0.0;
      if( flipAxis.at( iAxis ) ) position( iAxis ) *= -1.0;
    }

  }

};

// GenFit
#include <AbsBField.h>

namespace genfit{


  class NewNonUniformField : public AbsBField
  {
    public:

      NewNonUniformField( std::map<std::string, FieldMapInterpolator>&, double , double );
      ~NewNonUniformField() = default;

      TVector3 get(const TVector3&) const;
      void get(const double&, const double&, const double&, double&, double&, double&) const;

      FieldMapInterpolator GetMagneticFieldMap(std::string);

    private:

      std::map<std::string, FieldMapInterpolator> m_fieldMaps;
      double m_fieldUnit;
      double m_positionUnit;

  };


  inline NewNonUniformField::NewNonUniformField(
      std::map< std::string, FieldMapInterpolator >& fieldMaps
      , double fieldUnit
      , double positionUnit )
    : m_fieldMaps( fieldMaps ), m_fieldUnit( fieldUnit ), m_positionUnit( positionUnit )
  {
    // ...
  }

  inline TVector3 NewNonUniformField::get( const TVector3& pos ) const
  {
    // The units are naturally in cm for position, so there is no need
    // for conversion to millimeters.
    auto toROOT = []( const Vector3& values ) -> TVector3
    {
      return TVector3( values(0), values(1), values(2) );
    };

    TVector3 field(0.0,0.0,0.0);
    for( auto it = m_fieldMaps.begin(); it != m_fieldMaps.end(); it++ )
    {
      field += toROOT( it->second.GetField( Vector3(pos(0), pos(1), pos(2)) ) );
    }

    return field*m_fieldUnit;
  }

  inline void NewNonUniformField::get(
        const double& x, const double& y, const double& z
      , double& Bx, double& By, double& Bz
      ) const
  {
    auto field = this->get( TVector3(x,y,z) );
    Bx = field(0);
    By = field(1);
    Bz = field(2);
  }

}

#endif // FIELD_MAP_INTERPOLATOR_HH
