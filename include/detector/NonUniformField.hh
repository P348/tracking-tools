#ifndef NonUniformField_hh
#define NonUniformField_hh 1

// STD Library
#include <string>
#include <fstream>
#include <map>

// ROOT
#include <TVector3.h>

// GenFit
#include <AbsBField.h>

namespace utils{class MagneticFieldMap;}

namespace genfit{


  class NonUniformField : public AbsBField
  {
    public:
      NonUniformField(std::map<std::string, utils::MagneticFieldMap>&, double, double);
      ~NonUniformField();

      // virtual methods
      TVector3 get(const TVector3&) const;
      void get(const double&, const double&, const double&, double&, double&, double&) const;

      // parsing
      void CheckMapsRange(const TVector3&, std::string&);

      // get methods
      utils::MagneticFieldMap GetMagneticFieldMap(std::string);

    private:

      std::map<std::string, utils::MagneticFieldMap> fFieldMaps;
      double fFieldUnit;
      double fPositionUnit;

  };

}

#endif // NonUniformField_hh
