#ifndef TrackerGeneric_hh
#define TrackerGeneric_hh

// STD Library
#include <string>
#include <vector>
#include <utility>

// ROOT
#include "TVector3.h"

namespace Tracker{

  class Generic{
  
    public:

      Generic(std::string);
      virtual ~Generic();

      void SetTrackerName(std::string);
      void SetTrackerHit(std::pair<TVector3, double>);

      std::string GetTrackerName();
      std::vector<std::pair<TVector3,double> > GetTrackerHits();

      void ClearTrackerHits();

    protected:

      std::string fTrackerName;

      std::vector<std::pair<TVector3,double> > fTrackerHits;

    private:

      // ...

  };

} // end namespace Tracker

#endif // TrackerGeneric_hh
