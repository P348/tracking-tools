#ifndef IOStreamManager_hh
#define IOStreamManager_hh

// ROOT
#include "TTree.h"
#include "TList.h"

class IOStreamManager{

  public:
    IOStreamManager();
    virtual ~IOStreamManager();

    static IOStreamManager* GetInstance();

    // TTree
    virtual void SetInputTreeParser(TTree*) {;}
    virtual void SetOutputTreeParser(TTree*) {;}
    TTree* GetInputTree();
    TTree* GetOutputTree();
    TList* GetInputParam();

    virtual void ClearOutputTreeParser() {;}
    void Close();

  protected:

    TTree* fInputTree;
    TTree* fOutputTree;

    TList* fInputList;

    static IOStreamManager* fMgr;
};

#endif // IOStreamManager_hh
