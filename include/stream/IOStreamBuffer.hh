#ifndef IOStreamBuffer_hh
#define IOStreamBuffer_hh

#include <streambuf>
#include <fstream>
#include <string>


/**\brief Used to capture `std::stream` output
 *
 * IOStreamBuffer class originally implemented as Log4Cpp_StreambufRedirect in na64sw
 * Original author: R. Dusaev (Thanks a lot for sharing!)
 * Implemented in tracking-tools by B. Banto
 *
 * Set destination category and priority to use for stream's messages.
 *
 * \warning `sync()` does not necessarily empty the buffer!
 *
 */
class IOStreamBuffer : public std::streambuf {
  protected:
    ///\brief Overriden STL
    ///
    /// Redirects `c` message into file
    std::streamsize xsputn(const char_type * c, std::streamsize n) override;
    ///\brief Overriden STL
    ///
    /// Puts character
    int_type overflow(int_type ch) override;
    ///\brief Flushes buffered message(s)
    int sync() override;
    /// Concatenates newlined buffers
    std::string _buf;

    void _print_message_buffer();
  public:
    IOStreamBuffer();
    IOStreamBuffer(std::string);
    ~IOStreamBuffer();
    void SetSaveLog(bool);
    bool had_output() const { return this->_had_output; }
  private:
    IOStreamBuffer(const IOStreamBuffer&);
    IOStreamBuffer& operator=(const IOStreamBuffer&);
    std::string _pdNl; ///< platform-dependent newline
    std::ofstream _logfile; ///< output file
    std::string   _logfilename; ///< output file name
    bool _save2Log;
    bool _had_output;
};
            
#endif // IOStreamManager_hh
