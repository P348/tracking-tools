#ifndef ConfigFileParser_hh
#define ConfigFileParser_hh

// STD Library
#include <string>
#include <map>
#include <utility>

// Boost
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ptree_fwd.hpp>

// ROOT
#include "TVector2.h"
#include "TVector3.h"

class ConfigFileParser{

  public:

    ConfigFileParser();
    ConfigFileParser(std::string);
    ~ConfigFileParser();

    static ConfigFileParser* GetConfigFileParser();

    void SetConfigFile(std::string);
    void PrintConfigFile();

    // non-list types
    bool GetConfigValueWithABool(std::string);
    int GetConfigValueWithAnInt(std::string);
    double GetConfigValueWithADouble(std::string);
    std::string GetConfigValueWithAString(std::string);
    TVector2 GetConfigValueWithATVector2(std::string);
    TVector3 GetConfigValueWithATVector3(std::string);

    // list types
    bool GetConfigValueFromListWithABool(std::string, std::string);
    int GetConfigValueFromListWithAnInt(std::string, std::string);
    double GetConfigValueFromListWithADouble(std::string, std::string);
    std::string GetConfigValueFromListWithAString(std::string, std::string);
    TVector2 GetConfigValueFromListWithATVector2(std::string, std::string);
    TVector3 GetConfigValueFromListWithATVector3(std::string, std::string);


  private:

    static ConfigFileParser* fParser;

    std::string fConfigFile;
    boost::property_tree::ptree fPtree;

};

#endif // ConfigFileParser_hh
