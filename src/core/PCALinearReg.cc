#include "PCALinearReg.hh"
#include "FittedLine3D.hh"

PCALinearReg::PCALinearReg(std::vector<Eigen::RowVector3d>& Data, TRandom* rnd, double Sigma)
  : fData(Data),fRnd(rnd),fSigma(Sigma)
{
  // Data
  if(fRnd) this->AddSmearing();

  fMat = Eigen::MatrixXd(fData.size(), 3);
  for (unsigned int i = 0; i < fMat.rows(); i++) fMat.row(i) = fData.at(i);

  fUnit = Eigen::Vector3d::Zero();
  fChisq = 0.;
}

PCALinearReg::PCALinearReg(std::vector<Eigen::RowVector3d>&& Data, TRandom* rnd, double Sigma)
  : fData(Data),fRnd(rnd),fSigma(Sigma)
{
  // Data
  if(fRnd) this->AddSmearing();

  fMat = Eigen::MatrixXd(fData.size(), 3);
  for (unsigned int i = 0; i < fMat.rows(); i++) fMat.row(i) = fData.at(i);

  fUnit = Eigen::Vector3d::Zero();
  fChisq = 0.;
}

PCALinearReg::~PCALinearReg()
{
  // ...
}

void PCALinearReg::AddSmearing()
{
  for (unsigned int i = 0; i < fData.size(); i++)
    for (unsigned int j = 0; j < fData.at(i).size(); j++){
      fData.at(i)(j) = fRnd->Gaus(fData.at(i)(j), fSigma);
    }
}

void PCALinearReg::PreProcessData()
{
  // calculate column-average
  std::vector<double> Avg(fMat.cols(), 0.);
  for (unsigned int i = 0; i < Avg.size(); i++) Avg.at(i) = fMat.col(i).sum()/fMat.rows();

  // mean-normalise the matrix
  for (unsigned int i = 0; i < fMat.rows(); i++){
    for (unsigned int j = 0; j < fMat.cols(); j++)
      fMat(i,j) -= Avg.at(j);
  }
}

void PCALinearReg::CalculateUnit()
{
  // SVD
  Eigen::JacobiSVD<Eigen::MatrixXd> SVD(1./fMat.rows()*fMat.transpose()*fMat,
      Eigen::ComputeThinU | Eigen::ComputeThinV);

  // get results
  fUnit = SVD.matrixU().col(0);
}

void PCALinearReg::Fit()
{
  this->PreProcessData();
  this->CalculateUnit();
}

std::vector<Eigen::RowVector3d> PCALinearReg::GetData()
{
  return fData;
}

Eigen::MatrixXd PCALinearReg::GetDataMatrix()
{
  return fMat;
}

Eigen::Vector3d PCALinearReg::GetClusterSeed()
{
  Eigen::MatrixXd Mat = Eigen::MatrixXd(fData.size(), 3);
  for (unsigned int i = 0; i < Mat.rows(); i++) Mat.row(i) = fData.at(i);
  double X = Mat.col(0).mean();
  double Y = Mat.col(1).mean();
  double Z = Mat.col(2).mean();

  return Eigen::Vector3d(X,Y,Z);
}

Eigen::Vector3d PCALinearReg::GetUnitVector()
{
  return fUnit;
}

double PCALinearReg::GetChisqFit()
{
  FittedLine3D* TestLine = new FittedLine3D(GetClusterSeed(), GetUnitVector());

  // Reconstruct points
  fDataObs.resize(fData.size());
  fDataExp.resize(fData.size());
  for (size_t i = 0; i < fData.size(); i++) {
    Eigen::Vector3d Point = TestLine->GetExtrapolation(fData.at(i)(2));
    fDataObs.at(i) = Eigen::RowVector2d(Point(0), Point(1));
    fDataExp.at(i) = Eigen::RowVector2d(fData.at(i)(0), fData.at(i)(1));
  }

  for (size_t i = 0; i < fData.size(); i++) {
    fChisq += pow(fDataObs.at(i).norm()-fDataExp.at(i).norm(), 2.)/fDataExp.at(i).norm();
  }

  return fChisq;
}

void PCALinearReg::PrintFitInfo()
{
  std::cout << "Fit summary\n"
    << "- User input resolution: " << fSigma << "\n"
    << "- Number of points: " << fData.size() << "\n"
    << "- Chi-square of the track: " << GetChisqFit() << "\n"
    << "- True position vs. reco. position:\n";

  for (size_t i = 0; i < fDataExp.size(); i++) {
    std::cout <<"\tPoint #" << i << std::endl;
    std::cout << "\tx: " << fDataExp.at(i)(0) << " " << fDataObs.at(i)(0) <<std::endl;
    std::cout << "\ty: " << fDataExp.at(i)(1) << " " << fDataObs.at(i)(1) <<std::endl;
  }
}

