#include "FittedLine3D.hh"

// ROOT
#include "TMath.h"

FittedLine3D::FittedLine3D(Eigen::Vector3d& Seed, Eigen::Vector3d& Unit)
  : fSeed(Seed)
{
  fUnit = Unit;
}

FittedLine3D::FittedLine3D(Eigen::Vector3d&& Seed, Eigen::Vector3d&& Unit)
  : fSeed(Seed)
{
  fUnit = Unit;
}

FittedLine3D::~FittedLine3D()
{
  // ...
}

void FittedLine3D::SetUnitVector(Eigen::Vector3d& Unit)
{
  fUnit = Unit;
}

void FittedLine3D::SetUnitVector(Eigen::Vector3d&& Unit)
{
  fUnit = Unit;
}

void FittedLine3D::SetSeed(Eigen::Vector3d& Seed)
{
  fSeed = Seed;
}

void FittedLine3D::SetSeed(Eigen::Vector3d&& Seed)
{
  fSeed = Seed;
}

Eigen::Vector3d FittedLine3D::GetPosition(double t)
{
  return fSeed+fUnit*t;
}

Eigen::Vector3d FittedLine3D::GetUnitVector()
{
  return fUnit;
}

Eigen::Vector3d FittedLine3D::GetSeed()
{
  return fSeed;
}

Eigen::Vector3d FittedLine3D::GetExtrapolation(double Z)
{
  double t = (Z-fSeed(2))/fUnit(2);
  double X = fSeed(0)+fUnit(0)*t;
  double Y = fSeed(1)+fUnit(1)*t;

  return Eigen::Vector3d(X,Y,Z);
}

void FittedLine3D::PrintLine()
{
  std::cout << "Parametric line equation:\n";
  std::cout << "x(t) = " << fSeed.x() << " + " << fUnit.x() << "*t\n";
  std::cout << "y(t) = " << fSeed.y() << " + " << fUnit.y() << "*t\n";
  std::cout << "z(t) = " << fSeed.z() << " + " << fUnit.z() << "*t\n";
}

Eigen::Vector3d FittedLine3D::GetPOCA(FittedLine3D& line)
{

  bool choice = true;

  // define quantities
  Eigen::Vector3d u1 = this->GetUnitVector();
  Eigen::Vector3d u2 = line.GetUnitVector();
  Eigen::Vector3d u0;

  if (this->GetSeed().x() > line.GetSeed().y())
  {
    choice  = true;
    u0      = this->GetSeed()-line.GetSeed();
  }
  else
  {
    choice  = false;
    u0      = line.GetSeed()-this->GetSeed();
  }

  double t1 = 0.;
  double t2 = 0.;
  if (choice == true)
  {
    // calculate parameters from analytical solution
    t1 = (u1.dot(u2)*(u2.dot(u0))-u1.dot(u0))/(1-(u1.dot(u2))*(u1.dot(u2)));
    t2 = (-u1.dot(u2)*(u1.dot(u0))+u2.dot(u0))/(1-(u1.dot(u2))*(u1.dot(u2)));
  }
  else
  {
    t1 = (-u1.dot(u2)*(u2.dot(u0))+u1.dot(u0))/(1-(u1.dot(u2))*(u1.dot(u2)));
    t2 = (u1.dot(u2)*(u1.dot(u0))-u2.dot(u0))/(1-(u1.dot(u2))*(u1.dot(u2)));
  }

  // get POCA as mean
  // std::cout << "t1 = " << t1 << ", " << "t2 = " << t2 << std::endl;
  return 0.5*(this->GetPosition(t1)+line.GetPosition(t2));
}

