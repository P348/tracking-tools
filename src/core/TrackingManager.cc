#include "TrackingManager.hh"
#include "DetectorFactory.hh"
#include "TrackingGenFit.hh"
#include "TrackingCircle.hh"
#include "TrackingAnalytical.hh"
#include "TrackerGeneric.hh"

// STD Library
#include <iostream>
#include <utility>
#include <vector>
#include <algorithm>
#include <string>

// ROOT
#include "TVector3.h"


TrackingManager* TrackingManager::fMgrInstance = nullptr;

TrackingManager::TrackingManager() 
{
  // ...
}

TrackingManager::~TrackingManager() 
{
  if (fDetectorFactory) delete fDetectorFactory;
  if (fTrackingUser) delete fTrackingUser;
}

TrackingManager* TrackingManager::GetInstance() 
{
  if (!fMgrInstance) fMgrInstance = new TrackingManager();
  return fMgrInstance;
}

void TrackingManager::RegisterFactory(DetectorFactory* DetectorFactory)
{
  fDetectorFactory = DetectorFactory;
}

DetectorFactory* TrackingManager::GetFactory()
{
  return fDetectorFactory;
}

void TrackingManager::SetTrackingAlgorithm(TrackingUser* theTrackingUser)
{
  fTrackingUser = theTrackingUser;
}

TrackingUser* TrackingManager::GetTrackingAlgorithm()
{
  return fTrackingUser;
}

void TrackingManager::SetInputs(std::vector<std::pair<TVector3, double> > Points)

{
  fPoints = Points;
}

void TrackingManager::FindTrackCandidates()
{

  // merge the hits
  double dist = 1.;
  std::vector<std::pair<TVector3, double> > mergedHits;
  for (unsigned int i = 0; i < fPoints.size(); i++) {
    TVector3 hit1 = std::get<0>(fPoints.at(i));
    for (unsigned int j = i+1; j < fPoints.size(); j++) {
      TVector3 hit2 = std::get<0>(fPoints.at(j));
      if ((hit1-hit2).Mag() < dist) { // merge
        hit1 = 0.5*(hit1+hit2);
        fPoints.erase(fPoints.begin()+j);
      }
    }
    mergedHits.push_back(std::make_pair(hit1, std::get<1>(fPoints.at(i))));
  }

  // sort along z-axis
  std::sort(mergedHits.begin(), mergedHits.end(), 
      [](std::pair<TVector3,double> elem1, std::pair<TVector3,double> elem2)
      {return std::get<0>(elem1).Z() < std::get<0>(elem2).Z();}
      );

  // fill the detectors 
  double trackerDist = 50.; // distance btw. 2 trackers
  int id = 0;
  std::string name;
  for (unsigned int i = 0; i < mergedHits.size(); i++) {
    if (i == 0) { // first tracker
      id = 1;
      name = "Tracker_"+std::to_string(id);
      Tracker::Generic tracker(name.c_str());
      tracker.SetTrackerHit(mergedHits.at(i));
      fDetectorFactory->RegisterNewTracker(tracker.GetTrackerName(), tracker);
    }
    else {
      // get current detector and last registered hit
      Tracker::Generic tracker = fDetectorFactory->GetRegisteredTracker(name.c_str());
      double trackerHitZ = std::get<0>(tracker.GetTrackerHits().back()).Z();
      double newHitZ = std::get<0>(mergedHits.at(i)).Z();
      if (newHitZ-trackerHitZ < trackerDist) { // same tracker
        fDetectorFactory->AddHitToRegisteredTracker(name.c_str(), mergedHits.at(i));
      }
      else { // new tracker
        id++;
        name = "Tracker_"+std::to_string(id);
        Tracker::Generic tracker(name.c_str());
        tracker.SetTrackerHit(mergedHits.at(i));
        fDetectorFactory->RegisterNewTracker(tracker.GetTrackerName(), tracker);
      }
    }
  } // end of filling trackers

  // print all the registered trackers
  //fDetectorFactory->PrintListOfRegisteredTrackers(true);

}

void TrackingManager::FitTrackCandidates()
{
  if (fDetectorFactory->GetListOfRegisteredTrackers().size()) {
    fTrackingUser->SetTrackCandidates(fDetectorFactory->GetListOfRegisteredTrackers());
    fTrackingUser->FitTrackCandidates();
    fFittedTracks = fTrackingUser->GetFittedTracks();
    //TODO: fix this to work with analytical
    fRecoTracks = fTrackingUser->GetRecoTracks();
    fTrueTracks = fTrackingUser->GetTrueTracks();
  }
  else { // no candidates
    std::vector<TVector3> recoTracks;
    recoTracks.push_back(TVector3(-9999., -9999., -9999.));
    fFittedTracks.push_back(std::make_pair(-1.,9999.));
    fRecoTracks.push_back(recoTracks);
  }
}

void TrackingManager::SetFittedTracks(std::vector<std::pair<double,double> > FittedTracks)
{
  fFittedTracks = FittedTracks;
}

void TrackingManager::Clean()
{
  // Clean the factory
  fDetectorFactory->ClearListOfRegisteredTrackers();

  // Clear the inputs
  this->ClearInputs();

  // Clear the tracking
  fTrackingUser->Clean();
}

std::vector<std::pair<double,double> > TrackingManager::GetFittedTracks()
{
  return fFittedTracks;
}

std::vector<std::vector<TVector3> > TrackingManager::GetRecoTracks()
{
  return fRecoTracks;
}

std::vector<std::vector<TVector3> > TrackingManager::GetTrueTracks()
{
  return fTrueTracks;
}

void TrackingManager::ClearInputs()
{ 
  fTrackingUser->ClearInputs();
  fPoints.clear();
  fFittedTracks.clear();
  fRecoTracks.clear();
}

