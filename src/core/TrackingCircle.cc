#include "TrackingCircle.hh"
#include "PCALinearReg.hh"
#include "FittedLine3D.hh"
#include "MagnetMBPL.hh"

// STD Library
#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>
#include <stdlib.h>
#include <numeric>

// ROOT
#include "TVector3.h"
#include "TMath.h"

TrackingCircle::TrackingCircle() : TrackingUser()
{
  // ...
}

TrackingCircle::~TrackingCircle()
{
  // ...
}

void TrackingCircle::FitTrackCandidates()
{

  // prepare the hits for the tracking
  std::vector<std::pair<std::string, Tracker::Generic> > trackerList;
  std::copy(fCandidates.begin(), fCandidates.end(), std::back_inserter(trackerList));

  std::sort(trackerList.begin(), trackerList.end(),
      [](std::pair<std::string, Tracker::Generic> tracker1, 
        std::pair<std::string, Tracker::Generic> tracker2) {
      return (std::get<1>(tracker1)).GetTrackerHits().size() > (std::get<1>(tracker2)).GetTrackerHits().size();
      }
      );

  // sample the hits
  std::vector<std::vector<std::pair<TVector3, double> > > hitsList;
  for (size_t i = 0; i < trackerList.size(); i++) {
    std::vector<std::pair<TVector3, double> > hits;
    for (size_t j = 0; j < trackerList.at(i).second.GetTrackerHits().size(); j++) {
      hits.push_back(std::make_pair(trackerList.at(i).second.GetTrackerHits().at(j).first,trackerList.at(i).second.GetTrackerHits().at(j).second));
    }
    hitsList.push_back(hits);
    hits.clear();
  }
  std::vector<std::vector<std::pair<TVector3, double > > > hitsCandidates = this->GetCombinations(hitsList);

  std::vector<std::pair<double, double> > momAndChisq;

  // do the fit
  std::vector<std::vector<std::pair<TVector3, double> > >::iterator it; 
  for (it = hitsCandidates.begin(); it != hitsCandidates.end(); it++) {
    std::vector<std::pair<TVector3, double> > hits = *it;

    std::pair<double, double> track = this->FitTrack(hits);
    momAndChisq.push_back(track);

  }

  // save the results using chisq scoring
  if (momAndChisq.size() > 0) {
    std::sort(momAndChisq.begin(), momAndChisq.end(),
        [](std::pair<double,double> elem1, std::pair<double,double> elem2) {
        return std::get<1>(elem1) < std::get<1>(elem2);
        });
    for (size_t i = 0; i < momAndChisq.size(); i++) {
    fFittedTracks.push_back(std::make_pair(std::get<0>(momAndChisq.at(i)), std::get<1>(momAndChisq.at(i))));
    }
  }
  else{
    fFittedTracks.push_back(std::make_pair(-1.,9999.));
  }

}

std::pair<double, double> TrackingCircle::FitTrack(std::vector<std::pair<TVector3, double> >& hitsTrack)
{
  std::pair<double, double> momchisq;

  // get the magnet position 
  Magnet::MBPL* magnet = static_cast<Magnet::MBPL*>(fMagnet);
  double start = magnet->GetMBPLDimensions().GetZmin();
  double end = magnet->GetMBPLDimensions().GetZmax();

  // sort the hits 
  std::vector<TVector3> upstream;
  std::vector<TVector3> downstream;
  for (size_t i = 0; i < hitsTrack.size(); i++) {
    TVector3 hit = hitsTrack.at(i).first;
    if (hit.Z() < start) upstream.push_back(hit);
    else if (hit.Z() > end) downstream.push_back(hit);
  }

  // need at least two hit in each
  if (upstream.size() < 2 || downstream.size() < 2){
    momchisq = std::make_pair(-1., 9999.);
    return momchisq;
  }
  else {

    // get the parametric lines
    std::vector<Eigen::RowVector3d> Track1;
    for (size_t i = 0; i < upstream.size(); i++) {
      Eigen::RowVector3d Point(upstream.at(i).X(), upstream.at(i).Y(), upstream.at(i).Z());
      Track1.push_back(Point);
    }
    std::vector<Eigen::RowVector3d> Track2;
    for (size_t i = 0; i < downstream.size(); i++) {
      Eigen::RowVector3d Point(downstream.at(i).X(), downstream.at(i).Y(), downstream.at(i).Z());
      Track2.push_back(Point);
    }

    std::sort(Track1.begin(), Track1.end(), 
        [](Eigen::RowVector3d elem1, Eigen::RowVector3d elem2) {return elem1(2) < elem2(2);});
    std::sort(Track2.begin(), Track2.end(), 
        [](Eigen::RowVector3d elem1, Eigen::RowVector3d elem2) {return elem1(2) < elem2(2);});

    // do the fit
    PCALinearReg Fitter1(Track1, fRnd, fSmearing);
    PCALinearReg Fitter2(Track2, fRnd, fSmearing);
    Fitter1.Fit();
    Fitter2.Fit();

    FittedLine3D line1(Fitter1.GetClusterSeed(), Fitter1.GetUnitVector());
    FittedLine3D line2(Fitter2.GetClusterSeed(), Fitter2.GetUnitVector());
    Eigen::RowVector3d unit1 = Fitter1.GetUnitVector();
    Eigen::RowVector3d unit2 = Fitter2.GetUnitVector();

    // get the seed at the entrance and end of the magnet
    Eigen::RowVector3d seed1 = line1.GetExtrapolation(start);
    Eigen::RowVector3d seed2 = line2.GetExtrapolation(end);

    // rotate the unit vector and define the new lines (inverse rotation)
    auto Rot = [](double angle)
    {
      Eigen::Matrix2d R;
      R << TMath::Cos(angle), TMath::Sin(angle), -TMath::Sin(angle), TMath::Cos(angle);

      return R;
    };

    Eigen::Vector2d unit1_2(unit1(2), unit1(0));
    Eigen::Vector2d unit2_2(unit2(2), unit2(0));
    Eigen::Vector2d perpUnit1 = (unit1(2) < 0.) ? Rot(3*M_PI/2.)*unit1_2 : Rot(M_PI/2.)*unit1_2;
    Eigen::Vector2d perpUnit2 = (unit2(2) < 0.) ? Rot(3*M_PI/2.)*unit2_2 : Rot(M_PI/2.)*unit2_2;

    // find the intersection of the lines
    Eigen::RowVector3d unit1_(perpUnit1(1), unit1(1), perpUnit1(0));
    Eigen::RowVector3d unit2_(perpUnit2(1), unit2(1), perpUnit2(0));
    FittedLine3D line1_(seed1, unit1_);
    FittedLine3D line2_(seed2, unit2_);

    // get the reconstructed momentum
    Eigen::Vector3d POCA = line1_.GetPOCA(line2_);
    double radius = sqrt(pow(POCA.x()-seed1(0),2.)+pow(POCA.y()-seed1(1),2.)+pow(POCA.z()-seed1(2),2.));
    double momentum = 0.3*TMath::Abs(magnet->GetFieldMag())*radius*1.e-3;

    // calculate chisq (based on hit reco residuals)
    std::vector<double> residuals;
    for (size_t i = 0; i < Track1.size(); i++) 
    {
      Eigen::RowVector3d True = Track1.at(i);
      Eigen::RowVector3d Reco(line1.GetExtrapolation(True(2))(0),
          line1.GetExtrapolation(True(2))(1),
          line1.GetExtrapolation(True(2))(2));

      double residual = sqrt(pow(True(0)-Reco(0),2.)+pow(True(1)-Reco(1),2.)+pow(True(2)-Reco(2),2.));
      residuals.push_back(residual);
    }
    for (size_t i = 0; i < Track2.size(); i++) 
    {
      Eigen::RowVector3d True = Track2.at(i);
      Eigen::RowVector3d Reco(line2.GetExtrapolation(True(2))(0),
          line2.GetExtrapolation(True(2))(1),
          line2.GetExtrapolation(True(2))(2));

      double residual = sqrt(pow(True(0)-Reco(0),2.)+pow(True(1)-Reco(1),2.)+pow(True(2)-Reco(2),2.));
      residuals.push_back(residual);
    }

    double residual = std::accumulate(residuals.begin(), residuals.end(), 0.)/residuals.size();

    // result
    return std::make_pair(momentum, residual);
  }

}

