#include "TrackingUser.hh"
#include "TrackingGenFit.hh"
#include "MagnetMBPL.hh"
#include "NonUniformField.hh"
#include "IOStreamBuffer.hh"

// STD library
#include <iostream>
#include <algorithm>
#include <stdlib.h>
#include <math.h>

// ROOT
#include "TList.h"
#include "TParameter.h"
#include "TDatabasePDG.h"
#include "TParticlePDG.h"
#include "TGeoManager.h"
#include "TMath.h"

// GenFit
#include "ConstFieldBox.hh"
#include "ConstField2Box.hh"
#include "ConstField3Box.hh"
#include "ConstField4Box.hh"
#include "NonUniformField.hh"
#include <ConstField.h>
#include <Exception.h>
#include <FieldManager.h>
#include <KalmanFitter.h>
#include <KalmanFitterRefTrack.h>
#include <DAF.h>
#include <StateOnPlane.h>
#include <Track.h>
#include <TrackPoint.h>
#include <EventDisplay.h>
#include <MaterialEffects.h>
#include <RKTrackRep.h>
#include <TGeoMaterialInterface.h>
#include <PlanarMeasurement.h>
#include <mySpacepointDetectorHit.h>
#include <mySpacepointMeasurement.h>
#include <HelixTrackModel.h>

namespace Tracking{

  const double cm = 1.e-1;
  const double kG = 1.e1;

}

TrackingGenFit::TrackingGenFit() : TrackingUser()
{
  fMBPLField = true;
  fFitter = nullptr;
  fGeoManager = nullptr;
  fHitCov.ResizeTo(3,3);
  fHitCovA.ResizeTo(6,6);

  fInitDir = TVector3(0,0,1);

  fNTracks = 0;
  fTrackCandArray.SetClass("genfit::Track");

  // create streambufs
  std::string _logDbg = "debug.log";
  std::string _logErr = "error.log";
  std::string _logOut = "out.log";
  _rdbufDbg = new IOStreamBuffer(_logDbg);
  _rdbufErr = new IOStreamBuffer(_logErr);
  _rdbufOut = new IOStreamBuffer(_logOut);
  _rdbufDbg->SetSaveLog(false);
  _rdbufErr->SetSaveLog(false);
  _rdbufOut->SetSaveLog(false);

  // set buffers
  genfit::debugOut.rdbuf(_rdbufDbg);
  genfit::errorOut.rdbuf(_rdbufErr);
  genfit::printOut.rdbuf(_rdbufOut);
}

TrackingGenFit::~TrackingGenFit()
{
  genfit::FieldManager::getInstance()->destruct();
  if (fFitter) {delete fFitter; fFitter = nullptr;}
  //if (fGeoManager) {delete fGeoManager; fGeoManager = nullptr;}
  if (_rdbufDbg) {delete _rdbufDbg; _rdbufDbg = nullptr;}
  if (_rdbufErr) {delete _rdbufErr; _rdbufErr = nullptr;}
  if (_rdbufOut) {delete _rdbufOut; _rdbufOut = nullptr;}
}

void TrackingGenFit::SetPDG(double PDG) 
{
  fPDG = PDG;
}

void TrackingGenFit::SetNominalEnergy(double NominalEnergy)
{
  fNominalEnergy = NominalEnergy;
}

void TrackingGenFit::SetInitDir(TVector3 initDir)
{
  fInitDir = initDir;
}

void TrackingGenFit::SetGDMLManager(TGeoManager* GeoManager)
{
  fGeoManager = GeoManager;
}

void TrackingGenFit::SetNIter(const int NIter)
{
  fNIter = NIter;
}

void TrackingGenFit::PrintResults(std::vector<TVector3>& hitsTrack)
{

  if (fNTracks == 0) {
    std::cerr << "No genfit::Track defined, exiting..." << std::endl;
    exit(EXIT_FAILURE);
  }

  for (int i = 0; i < fNTracks; i++) {

    std::cout << "Reconstructed track number " << i << "..." << std::endl;
    genfit::Track* aTrack = static_cast<genfit::Track*>(fTrackCandArray.AddrAt(i));

    const genfit::FitStatus* status = aTrack->getFitStatus();
    std::cout << "Status:\n";
    status->Print();

    for (size_t i = 0; i < hitsTrack.size(); i++)
    {
      hitsTrack.at(i).Print();
    }
    std::cout << "Reconstructed points:\n";
    for (size_t i = 0; i < aTrack->getNumPoints(); i++)
    {
      aTrack->getFittedState(i).Print();
    }
  }

}

void TrackingGenFit::InitGeometry()
{

  genfit::MaterialEffects* matEffect = genfit::MaterialEffects::getInstance();
  matEffect->init(new genfit::TGeoMaterialInterface());

  // Options for material effects
  bool noEffects = false;
  if (noEffects) matEffect->setNoEffects(noEffects);
  else
  {
    matEffect->setMscModel("GEANE");
    matEffect->setEnergyLossBrems(true);
    matEffect->setNoiseBrems(true);
  }
}

void TrackingGenFit::InitMBPLField()
{
  if (fMagnets.size() == 1) {
    Magnet::MBPL* magnet = static_cast<Magnet::MBPL*>(fMagnets.at(0));
    double mag = magnet->GetFieldMag()*Tracking::kG;

    genfit::ConstFieldBox* field = 
      new genfit::ConstFieldBox(magnet->GetFieldDir().X()*mag, 
          magnet->GetFieldDir().Y()*mag, 
          magnet->GetFieldDir().Z()*mag,
          magnet->GetMBPLDimensions().GetXmin()*Tracking::cm,
          magnet->GetMBPLDimensions().GetXmax()*Tracking::cm,
          magnet->GetMBPLDimensions().GetYmin()*Tracking::cm,
          magnet->GetMBPLDimensions().GetYmax()*Tracking::cm,
          magnet->GetMBPLDimensions().GetZmin()*Tracking::cm,
          magnet->GetMBPLDimensions().GetZmax()*Tracking::cm);

    genfit::FieldManager* fieldMgr = genfit::FieldManager::getInstance();
    fieldMgr->init(field);

    // fieldMgr->getFieldVal(TVector3(0.,0.magnet->GetMBPLDimensions().GetZmax()-10.)).Print();
  }
  else if (fMagnets.size() == 2) {
    Magnet::MBPL* magnet1 = static_cast<Magnet::MBPL*>(fMagnets.at(0));
    Magnet::MBPL* magnet2 = static_cast<Magnet::MBPL*>(fMagnets.at(1));
    double mag1 = magnet1->GetFieldMag()*Tracking::kG;
    double mag2 = magnet2->GetFieldMag()*Tracking::kG;

    genfit::ConstField2Box* field = 
      new genfit::ConstField2Box(magnet1->GetFieldDir().X()*mag1, 
          magnet1->GetFieldDir().Y()*mag1, 
          magnet1->GetFieldDir().Z()*mag1,
          magnet2->GetFieldDir().X()*mag2, 
          magnet2->GetFieldDir().Y()*mag2, 
          magnet2->GetFieldDir().Z()*mag2,
          magnet1->GetMBPLDimensions().GetXmin()*Tracking::cm,
          magnet1->GetMBPLDimensions().GetXmax()*Tracking::cm,
          magnet1->GetMBPLDimensions().GetYmin()*Tracking::cm,
          magnet1->GetMBPLDimensions().GetYmax()*Tracking::cm,
          magnet1->GetMBPLDimensions().GetZmin()*Tracking::cm,
          magnet1->GetMBPLDimensions().GetZmax()*Tracking::cm,
          magnet2->GetMBPLDimensions().GetXmin()*Tracking::cm,
          magnet2->GetMBPLDimensions().GetXmax()*Tracking::cm,
          magnet2->GetMBPLDimensions().GetYmin()*Tracking::cm,
          magnet2->GetMBPLDimensions().GetYmax()*Tracking::cm,
          magnet2->GetMBPLDimensions().GetZmin()*Tracking::cm,
          magnet2->GetMBPLDimensions().GetZmax()*Tracking::cm);


    genfit::FieldManager* fieldMgr = genfit::FieldManager::getInstance();
    fieldMgr->init(field);

  }
  else if (fMagnets.size() == 3) {
    Magnet::MBPL* magnet1 = static_cast<Magnet::MBPL*>(fMagnets.at(0));
    Magnet::MBPL* magnet2 = static_cast<Magnet::MBPL*>(fMagnets.at(1));
    Magnet::MBPL* magnet3 = static_cast<Magnet::MBPL*>(fMagnets.at(2));
    double mag1 = magnet1->GetFieldMag()*Tracking::kG;
    double mag2 = magnet2->GetFieldMag()*Tracking::kG;
    double mag3 = magnet3->GetFieldMag()*Tracking::kG;

    genfit::ConstField3Box* field = 
      new genfit::ConstField3Box(magnet1->GetFieldDir().X()*mag1,
          magnet1->GetFieldDir().Y()*mag1,
          magnet1->GetFieldDir().Z()*mag1,
          magnet2->GetFieldDir().X()*mag2, 
          magnet2->GetFieldDir().Y()*mag2, 
          magnet2->GetFieldDir().Z()*mag2,
          magnet3->GetFieldDir().X()*mag3, 
          magnet3->GetFieldDir().Y()*mag3, 
          magnet3->GetFieldDir().Z()*mag3,
          magnet1->GetMBPLDimensions().GetXmin()*Tracking::cm, 
          magnet1->GetMBPLDimensions().GetXmax()*Tracking::cm,
          magnet1->GetMBPLDimensions().GetYmin()*Tracking::cm, 
          magnet1->GetMBPLDimensions().GetYmax()*Tracking::cm,
          magnet1->GetMBPLDimensions().GetZmin()*Tracking::cm, 
          magnet1->GetMBPLDimensions().GetZmax()*Tracking::cm,
          magnet2->GetMBPLDimensions().GetXmin()*Tracking::cm, 
          magnet2->GetMBPLDimensions().GetXmax()*Tracking::cm,
          magnet2->GetMBPLDimensions().GetYmin()*Tracking::cm, 
          magnet2->GetMBPLDimensions().GetYmax()*Tracking::cm,
          magnet2->GetMBPLDimensions().GetZmin()*Tracking::cm, 
          magnet2->GetMBPLDimensions().GetZmax()*Tracking::cm,
          magnet3->GetMBPLDimensions().GetXmin()*Tracking::cm, 
          magnet3->GetMBPLDimensions().GetXmax()*Tracking::cm,
          magnet3->GetMBPLDimensions().GetYmin()*Tracking::cm, 
          magnet3->GetMBPLDimensions().GetYmax()*Tracking::cm,
          magnet3->GetMBPLDimensions().GetZmin()*Tracking::cm, 
          magnet3->GetMBPLDimensions().GetZmax()*Tracking::cm);

    genfit::FieldManager* fieldMgr = genfit::FieldManager::getInstance();
    fieldMgr->init(field);

  }
  else if (fMagnets.size() == 4) {
    Magnet::MBPL* magnet1 = static_cast<Magnet::MBPL*>(fMagnets.at(0));
    Magnet::MBPL* magnet2 = static_cast<Magnet::MBPL*>(fMagnets.at(1));
    Magnet::MBPL* magnet3 = static_cast<Magnet::MBPL*>(fMagnets.at(2));
    Magnet::MBPL* magnet4 = static_cast<Magnet::MBPL*>(fMagnets.at(3));
    double mag1 = magnet1->GetFieldMag()*Tracking::kG;
    double mag2 = magnet2->GetFieldMag()*Tracking::kG;
    double mag3 = magnet3->GetFieldMag()*Tracking::kG;
    double mag4 = magnet4->GetFieldMag()*Tracking::kG;

    genfit::ConstField4Box* field = 
      new genfit::ConstField4Box(magnet1->GetFieldDir().X()*mag1,
          magnet1->GetFieldDir().Y()*mag1,
          magnet1->GetFieldDir().Z()*mag1,
          magnet2->GetFieldDir().X()*mag2, 
          magnet2->GetFieldDir().Y()*mag2, 
          magnet2->GetFieldDir().Z()*mag2,
          magnet3->GetFieldDir().X()*mag3, 
          magnet3->GetFieldDir().Y()*mag3, 
          magnet3->GetFieldDir().Z()*mag3,
          magnet4->GetFieldDir().X()*mag4, 
          magnet4->GetFieldDir().Y()*mag4, 
          magnet4->GetFieldDir().Z()*mag4,
          magnet1->GetMBPLDimensions().GetXmin()*Tracking::cm, 
          magnet1->GetMBPLDimensions().GetXmax()*Tracking::cm,
          magnet1->GetMBPLDimensions().GetYmin()*Tracking::cm, 
          magnet1->GetMBPLDimensions().GetYmax()*Tracking::cm,
          magnet1->GetMBPLDimensions().GetZmin()*Tracking::cm, 
          magnet1->GetMBPLDimensions().GetZmax()*Tracking::cm,
          magnet2->GetMBPLDimensions().GetXmin()*Tracking::cm, 
          magnet2->GetMBPLDimensions().GetXmax()*Tracking::cm,
          magnet2->GetMBPLDimensions().GetYmin()*Tracking::cm, 
          magnet2->GetMBPLDimensions().GetYmax()*Tracking::cm,
          magnet2->GetMBPLDimensions().GetZmin()*Tracking::cm, 
          magnet2->GetMBPLDimensions().GetZmax()*Tracking::cm,
          magnet3->GetMBPLDimensions().GetXmin()*Tracking::cm, 
          magnet3->GetMBPLDimensions().GetXmax()*Tracking::cm,
          magnet3->GetMBPLDimensions().GetYmin()*Tracking::cm, 
          magnet3->GetMBPLDimensions().GetYmax()*Tracking::cm,
          magnet3->GetMBPLDimensions().GetZmin()*Tracking::cm, 
          magnet3->GetMBPLDimensions().GetZmax()*Tracking::cm,
          magnet4->GetMBPLDimensions().GetXmin()*Tracking::cm, 
          magnet4->GetMBPLDimensions().GetXmax()*Tracking::cm,
          magnet4->GetMBPLDimensions().GetYmin()*Tracking::cm, 
          magnet4->GetMBPLDimensions().GetYmax()*Tracking::cm,
          magnet4->GetMBPLDimensions().GetZmin()*Tracking::cm, 
          magnet4->GetMBPLDimensions().GetZmax()*Tracking::cm);

    genfit::FieldManager* fieldMgr = genfit::FieldManager::getInstance();
    fieldMgr->init(field);

  }

}

void TrackingGenFit::InitNonUniformField()
{ 
  genfit::NonUniformField* field = new genfit::NonUniformField(fFieldMaps, Tracking::kG, Tracking::cm);

  genfit::FieldManager* fieldMgr = genfit::FieldManager::getInstance();
  fieldMgr->init(field);
}

void TrackingGenFit::InitMatrices(const unsigned int nMeasurements, const double detectorResolution)
{
  // covariance for spacepoints
  fHitCov.UnitMatrix();
  fHitCov *= detectorResolution*detectorResolution;

  // approximate covariance for spacepoints
  fHitCovA.UnitMatrix();
  for (size_t i = 0; i < 3; i++) 
    fHitCovA(i,i) *= detectorResolution*detectorResolution;
  for (size_t i = 3; i < 6; i++) 
    fHitCovA(i,i) *= TMath::Power(detectorResolution/nMeasurements/TMath::Sqrt(3.),2.);
}

void TrackingGenFit::InitGeomAndFields()
{
  // init Genfit
  this->InitGeometry();
  if (fMBPLField) {
    this->InitMBPLField();
  }
  else {
    this->InitNonUniformField();
  }
}

void TrackingGenFit::InitFitter(kFitterType&& Type)
{
  if (Type == TrackingGenFit::Kalman) 
    fFitter = new genfit::KalmanFitter(fNIter, 1.e-3);
  else if (Type == TrackingGenFit::KalmanRefTrack)
    fFitter = new genfit::KalmanFitterRefTrack(fNIter, 1.e-3);
  else if (Type == TrackingGenFit::DAF)
    fFitter = new genfit::DAF();
  else {
    std::cerr << "Fitter is not defined, exiting..." << std::endl;
    exit(EXIT_FAILURE);
  }
}

void TrackingGenFit::InitMeasurementsFactory()
{

  // init the factory
  fMyDetId = 1;
  auto myClass = TClass::GetClass("genfit::mySpacepointDetectorHit");
  fMyDetectorHitArray.SetClass(myClass);
  fFactory.addProducer(fMyDetId, 
      new genfit::MeasurementProducer<genfit::mySpacepointDetectorHit, 
      genfit::mySpacepointMeasurement>
      (&fMyDetectorHitArray));

}

void TrackingGenFit::FitTrackCandidates()
{

  // prepare the hits for the tracking
  std::vector<std::pair<std::string, Tracker::Generic> > trackerList;
  std::copy(fCandidates.begin(), fCandidates.end(), std::back_inserter(trackerList));

  std::sort(trackerList.begin(), trackerList.end(),
      [](std::pair<std::string, Tracker::Generic> tracker1, 
        std::pair<std::string, Tracker::Generic> tracker2) {
      return (std::get<1>(tracker1)).GetTrackerHits().size() > (std::get<1>(tracker2)).GetTrackerHits().size();
      }
      );

  // sample the hits
  std::vector<std::vector<std::pair<TVector3, double> > > hitsList;
  for (size_t i = 0; i < trackerList.size(); i++) {
    std::vector<std::pair<TVector3,double> > hits;
    for (size_t j = 0; j < trackerList.at(i).second.GetTrackerHits().size(); j++) {
      hits.push_back(std::make_pair(trackerList.at(i).second.GetTrackerHits().at(j).first,trackerList.at(i).second.GetTrackerHits().at(j).second));
    }
    hitsList.push_back(hits);
    hits.clear();
  }
  std::vector<std::vector<std::pair<TVector3, double> > > hitsCandidates = this->GetCombinations(hitsList);
  double detectorResolution = TMath::Sqrt(pow(fSmearing,2.)+pow(fSmearing,2.))*Tracking::cm;
  // fit the tracks using the hits candidates
  std::vector<std::vector<std::pair<TVector3, double> > >::iterator it;
  for (it = hitsCandidates.begin(); it != hitsCandidates.end(); it++) {
    std::vector<std::pair<TVector3, double> > hits = *it;
    std::vector<TVector3> hitsV;
    
    
    this->InitMatrices(hits.size(), detectorResolution);  

    std::pair<double, double> result = this->FitTrack(hits);
    if (fNTracks > 0) { // at least one track
      if (std::get<1>(result) != -1.) { // "good" track

        genfit::Track* aTrack = static_cast<genfit::Track*>(fTrackCandArray.AddrAt(fNTracks-1));
        Tracking::Track theTrack;

        theTrack.SetMom(std::get<0>(result));
        theTrack.SetChisquare(std::get<1>(result));
        theTrack.SetGenFitTrack(aTrack);
        // TODO: rewrite this
        for (unsigned int i = 0; i < hits.size(); i++) {
          hitsV.push_back(hits.at(i).first);
        }
        theTrack.SetTrueHits(hitsV);
        theTrack.SetRecoHits();

        fTrackCand.push_back(theTrack);

        // print results
        //PrintResults(hits);
      }
    }

  }

  // process results
  if (!fTrackCand.empty()) {

    // sort
    std::sort(fTrackCand.begin(), fTrackCand.end(), 
        [](const Tracking::Track& track1, const Tracking::Track& track2)
        {
        return track1.kChisquare < track2.kChisquare;
        }
        );

    // fill the reconstructed tracks
    for (auto iter = fTrackCand.begin(); iter != fTrackCand.end(); iter++)
    {
      const Tracking::Track& aTrack = *iter;
      fFittedTracks.push_back(std::make_pair(aTrack.kMomentum, aTrack.kChisquare));
      fRecoTracks.push_back(aTrack.kRecoHits);
      fTrueTracks.push_back(aTrack.kTrueHits);
    }
  }
  else {
    std::vector<TVector3> recoTrack;
    recoTrack.push_back(TVector3(-9999.,-9999.,-9999.));
    fFittedTracks.push_back(std::make_pair(-1.,9999.));
    fRecoTracks.push_back(recoTrack);
    fTrueTracks.push_back(recoTrack);
  }

#if 0
  // init visualisation and display
  genfit::EventDisplay* display = genfit::EventDisplay::getInstance();
  // EventDisplay crash on machines with Intel graphics, workaround:
  //   $ export LIBGL_ALWAYS_INDIRECT=1
  //   $ ./tracking.exe file.dat
  // Reference:
  //   https://root.cern.ch/phpBB3/viewtopic.php?t=21274

  bool showEvent = true;

  for (size_t i = 0; i < fNTracks; i++) {
    genfit::Track* aTrack = static_cast<genfit::Track*>(fTrackCandArray.AddrAt(i));
    display->addEvent(aTrack);
  }
  if (showEvent) display->open();
#endif

}

std::pair<double, double> TrackingGenFit::FitTrack(std::vector<std::pair<TVector3,double> >& hitsTrack)
{
  // verify the hit points are sorted
  std::sort(hitsTrack.begin(),hitsTrack.end(),
      [](const std::pair<TVector3,double>& v1, const std::pair<TVector3,double>& v2){return v1.first.Z() < v2.first.Z();});

  // track points
  const unsigned int nMeasurements = hitsTrack.size();

  // TrackCand
  genfit::TrackCand myCand;

  double detectorResolution = 999.0;
  for (size_t i = 0; i < nMeasurements; i++) {

    // TODO: check this
    // improve detector resolution estimate
    fHitCov.UnitMatrix();
    detectorResolution = hitsTrack.at(i).second*Tracking::cm;
    for (unsigned int i = 0; i < 3; i++) 
      fHitCov(i,i) *= detectorResolution*detectorResolution;
    
    new(fMyDetectorHitArray[i]) genfit::mySpacepointDetectorHit(hitsTrack.at(i).first*Tracking::cm, fHitCov);
    myCand.addHit(fMyDetId, i);

  }

  // TODO: check this
  // improve detector resolution estimate
  
  // define resolution as function of initial guess
  
  // initial guess
  TVector3 seedPos(hitsTrack.front().first*Tracking::cm);
  
  TVector3 seedMom = fInitDir; //default direction is (0,0,1)
  seedMom.SetMag(fNominalEnergy);
  
  // Error on the momentum coordinates
  double dP  = 6.0f;
  double dpX = dP*fInitDir.X();
  double dpY = dP*fInitDir.Y();
  double dpZ = dP*fInitDir.Z();

  //TMatrixDSym covSeed( 6 );
  fHitCovA.UnitMatrix();
  detectorResolution = hitsTrack.front().second*Tracking::cm;

  std::array< double, 6 > covParams
    = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
  covParams.at(0) = detectorResolution;
  covParams.at(1) = detectorResolution;
  
  // z-resolution is limited by survey measurement uncertainty (~1mm) and systematic uncertainty of definition of centre of sensitive plane
  /// TODO: Fine-tune this parameter
  covParams.at(2) = 20.;//std::sqrt(detectorResolution*detectorResolution+
                        //          detectorResolution*detectorResolution); //sqrt(2)*detReso
  
  covParams.at(3) = dpX;
  covParams.at(4) = dpY;
  covParams.at(5) = dpZ;

  for (size_t i = 0; i < 6; i++) 
    fHitCovA(i,i) = covParams[i]*covParams[i];
  
  /*
  fHitCovA.UnitMatrix();
  detectorResolution = hitsTrack.front().second*Tracking::cm;
  for (size_t i = 0; i < 3; i++) 
    fHitCovA(i,i) *= detectorResolution*detectorResolution;
  for (size_t i = 3; i < 6; i++) 
    fHitCovA(i,i) *= TMath::Power(detectorResolution/nMeasurements/TMath::Sqrt(3.),2.);
  */
  myCand.setPosMomSeedAndPdgCode(seedPos, seedMom, fPDG);
  myCand.setCovSeed(fHitCovA);

  new(fTrackCandArray[fNTracks]) genfit::Track(myCand, fFactory, new genfit::RKTrackRep(fPDG));
  genfit::Track* aTrack = static_cast<genfit::Track*>(fTrackCandArray.AddrAt(fNTracks));
  fNTracks++;

  // check 
  aTrack->checkConsistency();
  // do the fit
  fFitter->setMinIterations(5);
  fFitter->setMaxIterations(fNIter);
  fFitter->processTrack(aTrack);

  // check
  aTrack->checkConsistency();

  double momentum = aTrack->getFittedState().getMomMag();
  double chisq = aTrack->getFitStatus()->getChi2()/aTrack->getFitStatus()->getNdf();
  
  // check convergence
  if (!aTrack->getFitStatus()->isFitConvergedFully()) chisq = -1.;
  if (std::isnan(chisq)) chisq = -1.;
  if (std::isinf(chisq)) chisq = -1.;

  return std::make_pair(momentum, chisq);

}

bool TrackingGenFit::GetMBPLField()
{
  return fMBPLField;
}

std::vector<Tracking::Track> TrackingGenFit::GetReconstructedTracks()
{
  return fTrackCand;
}

void TrackingGenFit::SetMagnets(std::vector<Magnet::Generic*> Magnets)
{
  fMagnets = Magnets;
}

void TrackingGenFit::SetMagneticFieldMaps(kFieldMap& FieldMaps)
{
  fFieldMaps = FieldMaps;
}

void TrackingGenFit::SetMBPLField(bool MBPLField)
{
  fMBPLField = MBPLField;
}

void TrackingGenFit::Clean()
{

  fTrackCandArray.RemoveRange(0, fNTracks);
  fTrackCandArray.Clear("C");
  fNTracks = 0;

  fTrackCand.clear();

  // field and geometry
  genfit::FieldManager::getInstance()->destruct();

}
