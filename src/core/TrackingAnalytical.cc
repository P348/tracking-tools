#include "TrackingAnalytical.hh"
#include "MagnetMBPL.hh"

// STD Library
#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>
#include <numeric>
#include <stdlib.h>
#include <chrono>

TrackingAnalytical::TrackingAnalytical() : TrackingUser()
{
  // ...
}

TrackingAnalytical::~TrackingAnalytical()
{
  // ...
}

void TrackingAnalytical::FitTrackCandidates()
{

  // prepare the hits for the tracking
  std::vector<std::pair<std::string, Tracker::Generic> > trackerList;
  std::copy(fCandidates.begin(), fCandidates.end(), std::back_inserter(trackerList));

  std::sort(trackerList.begin(), trackerList.end(),
      [](std::pair<std::string, Tracker::Generic> tracker1, 
        std::pair<std::string, Tracker::Generic> tracker2) {
      return (std::get<1>(tracker1)).GetTrackerHits().size() > (std::get<1>(tracker2)).GetTrackerHits().size();
      }
      );

  // sample the hits
  std::vector<std::vector<std::pair<TVector3, double> > > hitsList;
  for (size_t i = 0; i < trackerList.size(); i++) {
    std::vector<std::pair<TVector3, double> > hits;
    for (size_t j = 0; j < trackerList.at(i).second.GetTrackerHits().size(); j++) {
      hits.push_back(std::make_pair(trackerList.at(i).second.GetTrackerHits().at(j).first,trackerList.at(i).second.GetTrackerHits().at(j).second ));
    }
    hitsList.push_back(hits);
    hits.clear();
  }
  std::vector<std::vector<std::pair<TVector3, double> > > hitsCandidates = this->GetCombinations(hitsList);

  // random seed
  //auto time1 = std::chrono::high_resolution_clock::now();
  //auto time2 = std::chrono::high_resolution_clock::now();
  //double seed = std::chrono::duration_cast<std::chrono::nanoseconds>(time2-time1).count();

  //gRandom->SetSeed(seed);
  if (fSmearing > 0.) { // apply smearing
    for (size_t i = 0; i < hitsCandidates.size(); i++) {
      for (size_t j = 0; j < hitsCandidates.at(i).size(); j++) {
        TVector3 hits = hitsCandidates.at(i).at(j).first;
        double resolution = hitsCandidates.at(i).at(j).second;
        hitsCandidates.at(i).at(j).first.SetX(fRnd->Gaus(hits.X(), resolution));
        hitsCandidates.at(i).at(j).first.SetY(fRnd->Gaus(hits.Y(), resolution));
      }
    }
  }

  std::vector<std::pair<double, double> > momAndChisq;

  // do the fit
  std::vector<std::vector<std::pair<TVector3, double> > >::iterator it; 
  for (it = hitsCandidates.begin(); it != hitsCandidates.end(); it++) {
    std::vector<std::pair<TVector3, double> > hits = *it;

    std::pair<double, double> track = this->FitTrack(hits);
    momAndChisq.push_back(track);

  }

  // save the results using chisq scoring
  if (momAndChisq.size() > 0) {
    std::sort(momAndChisq.begin(), momAndChisq.end(),
        [](std::pair<double,double> elem1, std::pair<double,double> elem2) {
        return std::get<1>(elem1) < std::get<1>(elem2);
        });
    for (size_t i = 0; i < momAndChisq.size(); i++) {
    fFittedTracks.push_back(std::make_pair(std::get<0>(momAndChisq.at(i)), std::get<1>(momAndChisq.at(i))));
    }
  }
  else{
    fFittedTracks.push_back(std::make_pair(-1.,9999.));
  }

}

std::pair<double, double> TrackingAnalytical::FitTrack(std::vector<std::pair<TVector3, double> >& hitsTrack)
{

  std::pair<double, double> momchisq;

  // get the magnet position 
  Magnet::MBPL* magnet = static_cast<Magnet::MBPL*>(fMagnet);
  double start = magnet->GetMBPLDimensions().GetZmin();
  double end = magnet->GetMBPLDimensions().GetZmax();
  double field = magnet->GetFieldMag();

  // sort the hits 
  std::vector<TVector3> upstream;
  std::vector<TVector3> downstream;
  for (size_t i = 0; i < hitsTrack.size(); i++) {
    TVector3 hit = hitsTrack.at(i).first;
    if (hit.Z() < start) upstream.push_back(hit);
    else if (hit.Z() > end) downstream.push_back(hit);
  }

  if (fNPlanes == 3) { // 3 hits

    bool checkTracking = true;
    if (!fBackPropagate) {
      if (upstream.size() < 2 || downstream.size() < 1) checkTracking = false;
    }
    if (fBackPropagate) {
      if (upstream.size() < 1 || downstream.size() < 2) checkTracking = false;
    }
    if (checkTracking == false) {
      momchisq = std::make_pair(-1., 9999.);
      return momchisq;
    }
    /*
    // need at least one upstream and two downstream
    if (upstream.size() < 2 || downstream.size() < 1){
      momchisq = std::make_pair(-1., 9999.);
      return momchisq;
    }
    */
    else {

      // generate combinations upstream
      std::vector<std::pair<TVector3, TVector3> > pairUpstream;
      for (size_t i = 0; i < upstream.size(); i++) 
        for (size_t j = i+1; j < upstream.size(); j++) 
          pairUpstream.push_back(std::make_pair(upstream.at(i), upstream.at(j)));

      // generate combinations downstream
      std::vector<std::pair<TVector3, TVector3> > pairDownstream;
      for (size_t i = 0; i < downstream.size(); i++) 
        for (size_t j = i+1; j < downstream.size(); j++) 
          pairDownstream.push_back(std::make_pair(downstream.at(i), downstream.at(j)));

      // do the fit
      double momentum;
      std::vector<double> momenta;

      if (fBackPropagate == false) {
        for (size_t i = 0; i < pairUpstream.size(); i++) {
          for (size_t j = 0; j < downstream.size(); j++) {
            momentum = this->SimpleTracking3(
                std::get<0>(pairUpstream.at(i)), std::get<1>(pairUpstream.at(i)), downstream.at(j),
                start, end, field);
            momenta.push_back(momentum);
          }
        }
      }
      
      else {
        for (size_t i = 0; i < pairDownstream.size(); i++) {
          for (size_t j = 0; j < upstream.size(); j++) {

            TVector3 pos1 = std::get<0>(pairDownstream.at(i)).Z() > std::get<1>(pairDownstream.at(i)).Z() ?
              std::get<0>(pairDownstream.at(i)) : std::get<1>(pairDownstream.at(i));
            TVector3 pos2 = std::get<0>(pairDownstream.at(i)).Z() > std::get<1>(pairDownstream.at(i)).Z() ?
              std::get<1>(pairDownstream.at(i)) : std::get<0>(pairDownstream.at(i));

            momentum = this->SimpleTracking3(pos1, pos2, upstream.at(j),
                end, start, field);
            //pos1.Print();
            //pos2.Print();
            //upstream.at(j).Print();
            //std::cout << end << "\t" << start << std::endl;
            momenta.push_back(momentum);
          }
        }
      }

      // calculate the chi-square
      double chisq = 0.;
      momentum = std::accumulate(momenta.begin(), momenta.end(), 0.)/momenta.size();

      for (size_t i = 0; i < momenta.size(); i++) {
        chisq += pow(momenta.at(i)-momentum, 2.)/(momentum*momentum);
      }

      momchisq = std::make_pair(momentum, chisq);
      return momchisq;
    }

  }
  else { // 4 hits

    // need at least two upstream and downstream
    if (upstream.size() < 2 || downstream.size() < 2){
      momchisq = std::make_pair(-1., 9999.);
      return momchisq;
    }
    else {

      // generate combinations upstream and downstream
      std::vector<std::pair<TVector3, TVector3> > pairUpstream;
      std::vector<std::pair<TVector3, TVector3> > pairDownstream;
      for (size_t i = 0; i < upstream.size(); i++) 
        for (size_t j = i+1; j < upstream.size(); j++) 
          pairUpstream.push_back(std::make_pair(upstream.at(i), upstream.at(j)));
      for (size_t i = 0; i < downstream.size(); i++) 
        for (size_t j = i+1; j < downstream.size(); j++) 
          pairDownstream.push_back(std::make_pair(downstream.at(i), downstream.at(j)));

      // do the fit
      double momentum;
      std::vector<double> momenta;
      for (size_t i = 0; i < pairUpstream.size(); i++) {
        for (size_t j = 0; j < pairDownstream.size(); j++) {
          momentum = this->SimpleTracking4(
              std::get<0>(pairUpstream.at(i)), std::get<1>(pairUpstream.at(i)),
              std::get<0>(pairDownstream.at(j)), std::get<1>(pairDownstream.at(j)),
              end-start, field);
          momenta.push_back(momentum);
        }
      }

      // calculate the chi-square
      double chisq = 0.;
      momentum = std::accumulate(momenta.begin(), momenta.end(), 0.)/momenta.size();

      for (size_t i = 0; i < momenta.size(); i++) {
        chisq += pow(momenta.at(i)-momentum, 2.)/(momentum*momentum);
      }

      momchisq = std::make_pair(momentum, chisq);
      return momchisq;
    }
  }

}

void TrackingAnalytical::SetNumberOfPlanes(unsigned int NPlanes)
{
  fNPlanes = NPlanes;
}

void TrackingAnalytical::SetBackPropagate(bool BackPropagate)
{
  fBackPropagate = BackPropagate;
}

double TrackingAnalytical::SimpleTracking3(
    const TVector3 p1,
    const TVector3 p2,
    const TVector3 p3,
    const double magb_z,
    const double mage_z,
    const double mag_field)
{

  // 3-point track momentum calculation
  // (the method make use of exact magnet position)
  // p1, p2 - coordinates of two hit points before magnet
  // p3 - coordinates of hit point after magnet
  // magb_z, mage_z - Z coordinates of begin and end of magnet
  // mag_field - magnetic field, Tesla
  // length and position units: mm

  const TVector3 del_12 = p2 - p1;
  const TVector3 del_23 = p3 - p2;

  // extrapolate p1 -> p2 line on p3 surface
  const double t1 = del_23.Z() / del_12.Z();
  const TVector3 P3 = p2 + t1 * del_12;

  const TVector3 del_23_new = p3 - P3;

  double mag_len = fabs(mage_z - magb_z);

  TVector3 del_23_correct = del_23_new * ((mage_z - magb_z)/2.0 / (p3.Z() - mage_z + (mage_z - magb_z)/2.0 ) );
  double rad = 0.5 * mag_len * sqrt(1 + pow(mag_len / del_23_correct.Perp(), 2));

  rad *= 0.001;

  const double q = 1.6E-19; // particle charge, C
  const double SI_GeV = 5.34E-19; // SI to GeV conversion = 1e9 * e * 1V / c , e - electron charge, c - speed of light
  const double C = mag_field * q / SI_GeV;

  const double mom = C * rad * cos(atan(del_12.Y()/del_12.Z()));

  return mom;
}

double TrackingAnalytical::SimpleTracking4(
    const TVector3 p1,
    const TVector3 p2,
    const TVector3 p3,
    const TVector3 p4,
    const double mag_len,
    const double mag_field)
{

  // simple track reconstruction
  // p1, p2 - coordinates of two hit points before magnet
  // p3, p4 - coordinates of two hit points after magnet
  // mag_len - length of magnetic field region along Z
  // mag_field - magnetic field, Tesla
  // length and position units: mm

  const TVector3 del_12 = p2 - p1;
  const TVector3 del_23 = p3 - p2;
  const TVector3 del_34 = p4 - p3;
  const TVector3 del_24 = p4 - p2;

  // extrapolate p1 -> p2 line on p3 surface
  const double t1 = del_23.Z() / del_12.Z();
  const TVector3 P3 = p2 + t1 * del_12;

  // extrapolate p1 -> p2 line on p4 surface
  const double t2 = del_24.Z() / del_12.Z();
  const TVector3 P4 = p2 + t2 * del_12;

  const TVector3 del_23_new = p3 - P3;
  const TVector3 del_24_new = p4 - P4;

  const double def_1 = sqrt(pow(del_12.Y(),2));
  const double def_2 = del_23_new.Perp();
  const double def_3 = del_24_new.Perp();

  const double q = 1.6E-19; // particle charge, C
  const double SI_GeV = 5.34E-19; // SI to GeV conversion = 1e9 * e * 1V / c , e - electron charge, c - speed of light
  const double C = mag_field * q / SI_GeV;

  const double rad = mag_len*1.E-3 * sqrt(1 + pow(del_34.Z() / (def_2-def_3), 2));
  const double mom = C * rad * cos(atan(def_1/del_12.Z()));

  return mom;
}

