#include "TrackingUser.hh"

TrackingUser::TrackingUser()
{
  // ...
}

TrackingUser::~TrackingUser()
{
  // ...
}

void TrackingUser::SetRnd(TRandom* rnd)
{
  fRnd=rnd;
}

void TrackingUser::SetSmearing(double Smearing)
{
  fSmearing = Smearing;
}

void TrackingUser::SetMagnet(Magnet::Generic* Magnet)
{
  fMagnet = Magnet;
}

void TrackingUser::SetTrackCandidates(std::map<std::string, Tracker::Generic> Candidates)
{
  fCandidates = Candidates;
}

std::vector<std::vector<std::pair<TVector3,double> > > 
TrackingUser::GetCombinations(const std::vector<std::vector<std::pair<TVector3,double> > >& detHits)
{
  std::vector<std::vector<std::pair<TVector3, double> > > combi1 = {{}};
  for (const auto& hits1 : detHits) {
    std::vector<std::vector<std::pair<TVector3, double> > > combi2;
    for (const auto& hits2 : combi1) {
      for (const auto& hit : hits1) {
        combi2.push_back(hits2);
        combi2.back().push_back(hit);
      }
    }
    combi1 = std::move(combi2);
  }
  return combi1;
}

std::vector<std::pair<double, double> >
TrackingUser::GetFittedTracks()
{
  return fFittedTracks;
}

std::vector<std::vector<TVector3> >
TrackingUser::GetRecoTracks()
{
  return fRecoTracks;
}

std::vector<std::vector<TVector3> >
TrackingUser::GetTrueTracks()
{
  return fTrueTracks;
}

void TrackingUser::ClearInputs()
{
  fCandidates.clear();
  fFittedTracks.clear();
  if (fTrueTracks.size() > 0) {
  fRecoTracks.clear();
  fTrueTracks.clear();
  }
}
