/* Copyright 2008-2010, Technische Universitaet Muenchen,
   Authors: Christian Hoeppner & Sebastian Neubert & Johannes Rauch

   This file is part of GENFIT.

   GENFIT is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published
   by the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   GENFIT is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with GENFIT.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "ConstField3Box.hh"
#include <iostream>

namespace genfit {

TVector3 ConstField3Box::get(const TVector3& pos) const {

  if((pos.X() >= _xmin1 && pos.X() <= _xmax1
     && pos.Y() >= _ymin1 && pos.Y() <= _ymax1
      && pos.Z() >= _zmin1 && pos.Z() <= _zmax1  )) {
    return field1_;
  }
  if((pos.X() >= _xmin2 && pos.X() <= _xmax2
      && pos.Y() >= _ymin2 && pos.Y() <= _ymax2
      && pos.Z() >= _zmin2 && pos.Z() <= _zmax2  )) {
    return field2_;
  } 
  if((pos.X() >= _xmin3 && pos.X() <= _xmax3
      && pos.Y() >= _ymin3 && pos.Y() <= _ymax3
      && pos.Z() >= _zmin3 && pos.Z() <= _zmax3  ) ){
    return field3_;
  }
  else{
    return TVector3(0,0,0);
  }
  
}

void ConstField3Box::get(const double& posX, const double& posY, const double& posZ, double& Bx, double& By, double& Bz) const {
  if((posX >= _xmin1 && posX <= _xmax1
     && posY >= _ymin1 && posY <= _ymax1
      && posZ >= _zmin1 && posZ <= _zmax1))
  {
    Bx = field1_.X();
    By = field1_.Y();
    Bz = field1_.Z();
    // std::cout << posX << ", " << posY << ", " << posZ << std::endl;
    // std::cout << _zmin << ", " << _zmax << ": " << Bx << ", " << By << ", " << Bz << std::endl;

  }
  else if((posX >= _xmin2 && posX <= _xmax2
      && posY >= _ymin2 && posY <= _ymax2
      && posZ >= _zmin2 && posZ <= _zmax2))
  {
    Bx = field2_.X();
    By = field2_.Y();
    Bz = field2_.Z();
    // std::cout << posX << ", " << posY << ", " << posZ << std::endl;
    // std::cout << _zmin << ", " << _zmax << ": " << Bx << ", " << By << ", " << Bz << std::endl;

  }
  else if((posX >= _xmin3 && posX <= _xmax3
      && posY >= _ymin3 && posY <= _ymax3
      && posZ >= _zmin3 && posZ <= _zmax3) ){

    Bx = field3_.X();
    By = field3_.Y();
    Bz = field3_.Z();
    // std::cout << posX << ", " << posY << ", " << posZ << std::endl;
    // std::cout << _zmin << ", " << _zmax << ": " << Bx << ", " << By << ", " << Bz << std::endl;

  }else{
    Bx = 0.;
    By = 0.;
    Bz = 0.;
  }   
}

} /* End of namespace genfit */
