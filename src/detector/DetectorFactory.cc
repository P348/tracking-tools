// User
#include "DetectorFactory.hh"
#include "TrackerMicromegas.hh"
#include "MagnetMBPL.hh"

// STD Library
#include <iostream>
#include <utility>
#include <stdlib.h>
#include <string>
#include <vector>

// ROOT
#include "TParameter.h"
#include "TMath.h"

DetectorFactory* DetectorFactory::fFactoryInstance = nullptr;

DetectorFactory::DetectorFactory()
{ 
  fGDMLGeoManager = nullptr;
  if (gGeoManager) {
    delete gGeoManager;
    gGeoManager = nullptr;
  }
}

DetectorFactory::~DetectorFactory()
{
  this->ClearListOfRegisteredTrackers();
  this->ClearListOfRegisteredMagnets();
 
  if (gGeoManager) delete gGeoManager;
  
}

DetectorFactory* DetectorFactory::GetInstance()
{
  if (!fFactoryInstance) fFactoryInstance = new DetectorFactory();
  return fFactoryInstance;
}

void DetectorFactory::SetFactoryName(std::string FactoryName)
{
  fFactoryName = FactoryName;
}

// Geometry
void DetectorFactory::SetGDMLFile(std::string GDMLFileName, int VerboseLevel)
{
  fGDMLFileName = GDMLFileName;

  if (VerboseLevel > 0) {
    std::cout << "GDML file used \033[1;33m" << fGDMLFileName << "\033[0m..." << std::endl;
  }
  if (!gGeoManager) {
    if (VerboseLevel > 0 ) std::cout << "Geometry initialised..." << std::endl;

    // TODO: compatibility with ROOT 6.24, to be checked against ROOT 6.22
    // (see ROOT forum issue #44519)
    TGeoManager::LockDefaultUnits(kFALSE);
    TGeoManager::SetDefaultUnits(TGeoManager::EDefaultUnits::kRootUnits);
    new TGeoManager("Geometry", "Tracking");
    TGeoManager::SetVerboseLevel(VerboseLevel);
    TGeoManager::Import(fGDMLFileName.c_str());
  }
}

TGeoManager* DetectorFactory::GetGDMLGeoManager()
{
  return gGeoManager;
}

// Trackers
void DetectorFactory::RegisterNewTracker(std::string name, Tracker::Generic tracker)
{
  std::pair<std::map<std::string, Tracker::Generic>::iterator, bool> insert 
    = fFactoryTrackers.insert(std::make_pair(name, tracker));
  if (!insert.second) {
    std::cerr << "Tracker  " << name << "  already registered, exiting." << std::endl;
    exit(EXIT_FAILURE);
  }
}

Tracker::Generic DetectorFactory::GetRegisteredTracker(std::string name)
{
  std::map<std::string, Tracker::Generic>::iterator it = fFactoryTrackers.find(name);
  if (it != fFactoryTrackers.end()) {
    return it->second;
  }
  else {
    std::cerr << "Tracker " << name << " not found, exiting." << std::endl;
    exit(EXIT_FAILURE);
  }
}

void DetectorFactory::AddHitToRegisteredTracker(std::string name, std::pair<TVector3, double> mergedHit)
{
  std::map<std::string, Tracker::Generic>::iterator it = fFactoryTrackers.find(name);
  if (it != fFactoryTrackers.end()) {
    it->second.SetTrackerHit(mergedHit);
  }
  else {
    std::cerr << "Tracker " << name << " not found, exiting." << std::endl;
    exit(EXIT_FAILURE);
  }
}

void DetectorFactory::PrintListOfRegisteredTrackers(bool printHits)
{
  std::map<std::string, Tracker::Generic>::iterator it;
  for (it = fFactoryTrackers.begin(); it != fFactoryTrackers.end(); it++) {
    std::cout << it->first << std::endl;
    if (printHits) {
      for (auto hits : it->second.GetTrackerHits()) {
        std::get<0>(hits).Print();
      }
    }
  }
}

void DetectorFactory::ClearRegisteredTrackersHits()
{
  std::map<std::string, Tracker::Generic>::iterator it;
  for (it = fFactoryTrackers.begin(); it != fFactoryTrackers.end(); it++) {
    it->second.ClearTrackerHits();
  }
}

std::map<std::string, Tracker::Generic> DetectorFactory::GetListOfRegisteredTrackers()
{
  return fFactoryTrackers;
}

void DetectorFactory::ClearListOfRegisteredTrackers()
{
  fFactoryTrackers.clear();

  std::map<std::string, Tracker::Generic>::iterator it;
  for (it = fFactoryTrackers.begin(); it != fFactoryTrackers.end(); it++) {
    fFactoryTrackers.erase(it);
  }

}

// Magnets
void DetectorFactory::RegisterNewMagnet(std::string name, Magnet::Generic* magnet)
{
  std::pair<std::map<std::string, Magnet::Generic*>::iterator, bool> insert 
    = fFactoryMagnets.insert(std::make_pair(name, magnet));
  if (!insert.second) {
    std::cerr << "Magnet  " << name << "  already registered, exiting." << std::endl;
    exit(EXIT_FAILURE);
  }
}

void DetectorFactory::RegisterListOfMagnets(std::vector<Magnet::Generic*>& magnets)
{
  for (auto it = magnets.begin(); it != magnets.end(); it++) {
    std::pair<std::map<std::string, Magnet::Generic*>::iterator, bool> insert 
      = fFactoryMagnets.insert(std::make_pair((*it)->GetMagnetName(), *it));
    if (!insert.second) {
      std::cerr << "Magnet  " << (*it)->GetMagnetName() << "  already registered, exiting." << std::endl;
      exit(EXIT_FAILURE);
    }
  }
}

Magnet::Generic* DetectorFactory::GetRegisteredMagnet(std::string name)
{
  std::map<std::string, Magnet::Generic*>::iterator it = fFactoryMagnets.find(name);
  if (it != fFactoryMagnets.end()) {
    return it->second;
  }
  else {
    std::cerr << "Magnet " << name << " not found, exiting." << std::endl;
    exit(EXIT_FAILURE);
  }
}

void DetectorFactory::PrintListOfRegisteredMagnets()
{
  std::map<std::string, Magnet::Generic*>::iterator it;
  for (it = fFactoryMagnets.begin(); it != fFactoryMagnets.end(); it++) {
    std::cout << it->first << std::endl;
  }
}

std::map<std::string, Magnet::Generic*> DetectorFactory::GetListOfRegisteredMagnets()
{
  return fFactoryMagnets;
}

void DetectorFactory::ClearListOfRegisteredMagnets()
{
  //TODO: check for proper deletion of magnets for usage of new keywork
  //where option std::map::erase seems more appropriate

  fFactoryMagnets.clear();
  
  std::map<std::string, Magnet::Generic*>::iterator it;
  for (it = fFactoryMagnets.begin(); it != fFactoryMagnets.end(); it++) {
    delete it->second;
    fFactoryMagnets.erase(it);
  }
  
}


