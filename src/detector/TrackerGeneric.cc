#include "TrackerGeneric.hh"

namespace Tracker{

  Generic::Generic(std::string TrackerName)
  {
    fTrackerName = TrackerName;
  }

  Generic::~Generic()
  {
    // ...
  }

  void Generic::SetTrackerName(std::string TrackerName)
  {
    fTrackerName = TrackerName;
  }

  void Generic::SetTrackerHit(std::pair<TVector3,double> TrackerHit)
  {
    fTrackerHits.push_back(TrackerHit);
  }

  void Generic::ClearTrackerHits()
  {
    fTrackerHits.clear();
  }

  std::string Generic::GetTrackerName()
  {
    return fTrackerName;
  }

  std::vector<std::pair<TVector3,double> > Generic::GetTrackerHits()
  {
    return fTrackerHits;
  }


} // end namespace Tracker
