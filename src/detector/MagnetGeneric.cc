#include "MagnetGeneric.hh"

namespace Magnet{

  Generic::Generic(std::string MagnetName)
  {
    fMagnetName = MagnetName;
  }

  Generic::~Generic()
  {
    // ...
  }

  void Generic::SetMagnetName(std::string MagnetName)
  {
    fMagnetName = MagnetName;
  }

  void Generic::SetFieldMag(double FieldMag)
  {
    fFieldMag = FieldMag;
  }

  void Generic::SetFieldDir(TVector3 FieldDir)
  {
    fFieldDir = FieldDir;
  }

  std::string Generic::GetMagnetName()
  {
    return fMagnetName;
  }

  double Generic::GetFieldMag()
  {
    return fFieldMag;
  }

  TVector3 Generic::GetFieldDir()
  {
    return fFieldDir;
  }

} // end namespace Magnet
