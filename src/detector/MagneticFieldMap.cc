#include "MagneticFieldMap.hh"

// STD Library
#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <algorithm>

namespace utils{

  MagneticFieldMap::MagneticFieldMap()
  {
    fGlobalShift.SetXYZ(0.0,0.0,0.0);
    fGlobalRot.SetXYZ(0.0,0.0,0.0);
    fGlobalScaling = 1.0;
  }

  MagneticFieldMap::MagneticFieldMap(std::string Name)
    : fName(Name)
  {
    fGlobalShift.SetXYZ(0.0,0.0,0.0);
    fGlobalRot.SetXYZ(0.0,0.0,0.0);
    fGlobalScaling = 1.0;
  }

  MagneticFieldMap::~MagneticFieldMap()
  {
    // ...
  }
  MagneticFieldMap MagneticFieldMap::operator=(const MagneticFieldMap& aMap)
  {
    fName = aMap.GetName();
    fGrid = aMap.GetGrid();
    return *this;
  }
  void MagneticFieldMap::SetVerboseLevel(unsigned int Level)
  {
    fVerboseLvl = Level;
  }

  void MagneticFieldMap::SetGlobalShift(const TVector3& Shift)
  {
    // Convention: units are converted from degree to rad for rotation, mm to cm for
    // translations
    fGlobalShift.SetX(fGlobalShift.X()+Shift.X());
    fGlobalShift.SetY(fGlobalShift.Y()+Shift.Y());
    fGlobalShift.SetZ(fGlobalShift.Z()+Shift.Z());
  }

  void MagneticFieldMap::SetGlobalRotation(const TVector3& Rot)
  {
    // Convention: units are converted from degree to rad for rotation, mm to cm for
    // translations
    fGlobalRot.SetX(fGlobalRot.X()+Rot.X());
    fGlobalRot.SetY(fGlobalRot.Y()+Rot.Y());
    fGlobalRot.SetZ(fGlobalRot.Z()+Rot.Z());
    fGlobalRot *= M_PI/180.;
  }

  void MagneticFieldMap::SetGlobalScaling(double scaling)
  {
    fGlobalScaling = scaling;
  }

  void MagneticFieldMap::InitFieldGrid(std::string fileName)
  {

    // I/O stream
    std::ifstream inFile(fileName, std::ios_base::in);
    std::string line;

    bool GridParameterCheck = false;
    double X,Y,Z;
    double Bx,By,Bz;

    TVector3 shift;

    while (getline(inFile, line)) 
    {

      // fill-in the grid parameters
      if (!GridParameterCheck) {
        inFile >> fGrid.pXAxis.pMin >> fGrid.pXAxis.pMax >> fGrid.pXAxis.pNPoints;
        inFile >> fGrid.pYAxis.pMin >> fGrid.pYAxis.pMax >> fGrid.pYAxis.pNPoints;
        inFile >> fGrid.pZAxis.pMin >> fGrid.pZAxis.pMax >> fGrid.pZAxis.pNPoints;
        
        // center the map to (0.0, 0.0, 0.0) 
        const double corrX = -0.5*(fGrid.pXAxis.pMin+fGrid.pXAxis.pMax);
        const double corrY = -0.5*(fGrid.pYAxis.pMin+fGrid.pYAxis.pMax);
        const double corrZ = -0.5*(fGrid.pZAxis.pMin+fGrid.pZAxis.pMax);

        fGrid.pXAxis.ShiftAxis(corrX);
        fGrid.pYAxis.ShiftAxis(corrY);
        fGrid.pZAxis.ShiftAxis(corrZ);

        fGrid.pXAxis.CalculateStep();
        fGrid.pYAxis.CalculateStep();
        fGrid.pZAxis.CalculateStep();

        // init the field cuboid
        fCuboid.SetVertex(0, fGrid.pXAxis.pMax, fGrid.pYAxis.pMin, fGrid.pZAxis.pMin);
        fCuboid.SetVertex(1, fGrid.pXAxis.pMin, fGrid.pYAxis.pMin, fGrid.pZAxis.pMin);
        fCuboid.SetVertex(2, fGrid.pXAxis.pMin, fGrid.pYAxis.pMin, fGrid.pZAxis.pMax);
        fCuboid.SetVertex(3, fGrid.pXAxis.pMax, fGrid.pYAxis.pMin, fGrid.pZAxis.pMax);
        fCuboid.SetVertex(4, fGrid.pXAxis.pMax, fGrid.pYAxis.pMax, fGrid.pZAxis.pMin);
        fCuboid.SetVertex(5, fGrid.pXAxis.pMin, fGrid.pYAxis.pMax, fGrid.pZAxis.pMin);
        fCuboid.SetVertex(6, fGrid.pXAxis.pMin, fGrid.pYAxis.pMax, fGrid.pZAxis.pMax);
        fCuboid.SetVertex(7, fGrid.pXAxis.pMax, fGrid.pYAxis.pMax, fGrid.pZAxis.pMax);

        //ROOT::Math::EulerAngles rotation(fGlobalRot.X(), fGlobalRot.Y(), fGlobalRot.Z());
        ROOT::Math::RotationZYX rotation(fGlobalRot.Z(), fGlobalRot.Y(), fGlobalRot.X());
        ROOT::Math::Translation3D translation(fGlobalShift.X(), fGlobalShift.Y(), fGlobalShift.Z());
        fCuboid.SetTransformation(rotation, translation);

        GridParameterCheck = true;
      } 
      // fill-in the node values
      else {
        shift.SetXYZ(0.,0.,0.);
        shift.SetX(fGrid.pXAxis.pMin);
        shift.SetY(fGrid.pYAxis.pMin);
        shift.SetZ(fGrid.pZAxis.pMin);
        for (unsigned int i = 0; i < fGrid.pXAxis.pNPoints; i++) {
          for (unsigned int j = 0; j < fGrid.pYAxis.pNPoints; j++) {
            for (unsigned int k = 0; k < fGrid.pZAxis.pNPoints; k++) {
              inFile >> X >> Y >> Z >> Bx >> By >> Bz;
              //std::cout << X << " "<< Y << " " << Z << ", " << Bx << " " << By << " " << By << "\n";
              TVector3 PosAtNode(X+shift.X(),Y+shift.Y(),Z+shift.Z());
              TVector3 FieldAtNode(Bx,By,Bz);
              FieldAtNode *= fGlobalScaling;
              fGrid.pGrid.push_back(std::make_pair(PosAtNode,FieldAtNode));
            }
          }
        }
      }
    } // end of loop over the stream

    inFile.close();

  }

  void MagneticFieldMap::PrintGrid()
  {

    utils::GridAxis XAxis = fGrid.pXAxis;
    utils::GridAxis YAxis = fGrid.pYAxis;
    utils::GridAxis ZAxis = fGrid.pZAxis;
    std::cout << XAxis.pMin << " " << XAxis.pMax << " " << XAxis.pStep << " (" << XAxis.pNPoints << ")" << std::endl;
    std::cout << YAxis.pMin << " " << YAxis.pMax << " " << YAxis.pStep << " (" << YAxis.pNPoints << ")" <<  std::endl;
    std::cout << ZAxis.pMin << " " << ZAxis.pMax << " " << ZAxis.pStep << " (" << ZAxis.pNPoints << ")" <<  std::endl;

    if (fVerboseLvl > 1) {
      for (unsigned int i = 0; i < fGrid.pXAxis.pNPoints; i++) {
        for (unsigned int j = 0; j < fGrid.pYAxis.pNPoints; j++) {
          for (unsigned int k = 0; k < fGrid.pZAxis.pNPoints; k++) {
            std::get<0>(GetGridPointValue(TVector3(i,j,k))).Print();
          }
        }
      }
    }

  }

  bool MagneticFieldMap::CheckRange(const TVector3& Pos) const
  {

    // convert to cm to ensure compatibility with ROOT translation
    ROOT::Math::XYZVector point(Pos.X(), Pos.Y(), Pos.Z());

    return fCuboid.IsWithinBoundary(point);
  }

  utils::kGridPoint MagneticFieldMap::GetGridPointValue(const TVector3& Index) const
  {
    utils::GridAxis XAxis = fGrid.pXAxis;
    utils::GridAxis YAxis = fGrid.pYAxis;

    // check for out-of-range
    TVector3 Index_ = Index;
    if (Index.X() < 0) Index_.SetX(2);
    if (Index.Y() < 0) Index_.SetY(2);
    if (Index.Z() < 0) Index_.SetZ(2);

    int index
      = (XAxis.pNPoints*Index_.X()+Index_.Y())*YAxis.pNPoints+Index_.Z();
    // check
    utils::kGridPoint point;
    try {
      point = fGrid.pGrid.at(index);
      return point;
    }
    catch (const std::out_of_range& exception) {
      std::cerr << "WARNING: out-of-range element in MagneticFieldMap::GetGridPointValue(const TVector3&), default value assigned !" << std::endl;

      utils::GridAxis ZAxis = fGrid.pZAxis;
      TVector3 pos(XAxis.pMin+3*XAxis.pStep,
          YAxis.pMin+3*YAxis.pStep,
          ZAxis.pMin+3*ZAxis.pStep);
      TVector3 field(0.,0.,0.);

      return std::make_pair(pos,field);
    }
  }

  utils::kGridPoint MagneticFieldMap::GetGridPointValue(const TVector3&& Index) const
  {
    utils::GridAxis XAxis = fGrid.pXAxis;
    utils::GridAxis YAxis = fGrid.pYAxis;

    // check for out-of-range
    TVector3 Index_ = Index;
    if (Index.X() < 0) Index_.SetX(2);
    if (Index.Y() < 0) Index_.SetY(2);
    if (Index.Z() < 0) Index_.SetZ(2);

    int index
      = (XAxis.pNPoints*Index_.X()+Index_.Y())*YAxis.pNPoints+Index_.Z();
    // check
    utils::kGridPoint point;
    try {
      point = fGrid.pGrid.at(index);
      return point;
    }
    catch (const std::out_of_range& exception) {
      std::cerr << "WARNING: out-of-range element in MagneticFieldMap::GetGridPointValue(const TVector3&&), default value assigned !" << std::endl;

      utils::GridAxis ZAxis = fGrid.pZAxis;
      TVector3 pos(XAxis.pMin+3*XAxis.pStep,
          YAxis.pMin+3*YAxis.pStep,
          ZAxis.pMin+3*ZAxis.pStep);
      TVector3 field(0.,0.,0.);

      return std::make_pair(pos,field);
    }
  }

  TVector3 MagneticFieldMap::GetGridPointIndex(const TVector3& Pos) const
  {
    utils::GridAxis XAxis = fGrid.pXAxis;
    utils::GridAxis YAxis = fGrid.pYAxis;
    utils::GridAxis ZAxis = fGrid.pZAxis;

    int IdxX = static_cast<int>((Pos.X()-XAxis.pMin)/XAxis.pStep);
    int IdxY = static_cast<int>((Pos.Y()-YAxis.pMin)/YAxis.pStep);
    int IdxZ = static_cast<int>((Pos.Z()-ZAxis.pMin)/ZAxis.pStep);

    TVector3 Index(IdxX,IdxY,IdxZ);
    // check for out-of-range
    try {
      GetGridPointValue(Index);
    }
    catch (const std::out_of_range& exception) {
      std::cerr << "WARNING: out-of-range element in MagneticFieldMap::GetGridPointIndex(const TVector3&), default value assigned !" << std::endl;
      return TVector3(2,2,2);
    }

    return Index;

  }

  TVector3 MagneticFieldMap::GetGridPointIndex(const TVector3&& Pos) const
  {
    utils::GridAxis XAxis = fGrid.pXAxis;
    utils::GridAxis YAxis = fGrid.pYAxis;
    utils::GridAxis ZAxis = fGrid.pZAxis;

    int IdxX = static_cast<int>((Pos.X()-XAxis.pMin)/XAxis.pStep);
    int IdxY = static_cast<int>((Pos.Y()-YAxis.pMin)/YAxis.pStep);
    int IdxZ = static_cast<int>((Pos.Z()-ZAxis.pMin)/ZAxis.pStep);

    TVector3 Index(IdxX,IdxY,IdxZ);
    // check for out-of-range
    try {
      GetGridPointValue(Index);
    }
    catch (const std::out_of_range& exception) {
      std::cerr << "WARNING: out-of-range element in MagneticFieldMap::GetGridPointIndex(const TVector3&&), default value assigned !" << std::endl;
      return TVector3(2,2,2);
    }

    return Index;

  }

  utils::kGridPoint MagneticFieldMap::GetClosestNeighbour(const TVector3& Pos) const
  {
    // get first guess
    TVector3 Idx = this->GetGridPointIndex(Pos);

    // closest neighbour correction
    std::vector<std::pair<int, double>> diffX, diffY, diffZ;
    double diffx, diffy, diffz;
    for (int k = -1; k < 2; k++) {
      diffx
        = std::get<0>(this->GetGridPointValue(TVector3(Idx.X()+k, Idx.Y(), Idx.Z()))).X()-Pos.X();
      diffy
        = std::get<0>(this->GetGridPointValue(TVector3(Idx.X(), Idx.Y()+k, Idx.Z()))).Y()-Pos.Y();
      diffz
        = std::get<0>(this->GetGridPointValue(TVector3(Idx.X(), Idx.Y(), Idx.Z()+k))).Z()-Pos.Z();

      diffX.push_back(std::make_pair(k, diffx));
      diffY.push_back(std::make_pair(k, diffy));
      diffZ.push_back(std::make_pair(k, diffz));
    }

    // correct
    std::sort(diffX.begin(), diffX.end(), 
        [](const std::pair<int, double> val1, const std::pair<int, double> val2) 
        {
        return std::fabs(std::get<1>(val1)) < std::fabs(std::get<1>(val2));
        });
    std::sort(diffY.begin(), diffY.end(), 
        [](const std::pair<int, double> val1, const std::pair<int, double> val2)
        {
        return std::fabs(std::get<1>(val1)) < std::fabs(std::get<1>(val2));
        });
    std::sort(diffZ.begin(), diffZ.end(), 
        [](const std::pair<int, double> val1, const std::pair<int, double> val2)
        {
        return std::fabs(std::get<1>(val1)) < std::fabs(std::get<1>(val2));
        });

    return this->GetGridPointValue(TVector3(Idx.X()+std::get<0>(diffX.front()),
          Idx.Y()+std::get<0>(diffY.front()),
          Idx.Z()+std::get<0>(diffZ.front())));

  }

  TVector3 MagneticFieldMap::InterpolateFieldValue(const TVector3& Pos) const
  {
    // TODO: check
    ROOT::Math::XYZPoint global(Pos.X(),Pos.Y(),Pos.Z());
    //std::cout << "GLOBAL = " << global.x() << ", " << global.y() << ", " << global.z() << std::endl;
    ROOT::Math::XYZPoint local = fCuboid.GetTransformation().Inverse()*global;
    //std::cout << "LOCAL = " << local.x() << ", " << local.y() << ", " << local.z() << std::endl;
    //getchar();

    /*
    ROOT::Math::Rotation3D mrot = fCuboid.GetTransformation().Rotation();
    ROOT::Math::XYZVector v,u,w;
    mrot.GetComponents(v,u,w);
    std::cout << "Rotation: \n"; 
    std::cout << v.X() << "," << u.X() << "," << w.X() << std::endl;
    std::cout << v.Y() << "," << u.Y() << "," << w.Y() << std::endl;
    std::cout << v.Z() << "," << u.Z() << "," << w.Z() << std::endl;
    mrot.Invert();
    mrot.GetComponents(v,u,w);
    std::cout << "Rotation: \n"; 
    std::cout << v.X() << "," << u.X() << "," << w.X() << std::endl;
    std::cout << v.Y() << "," << u.Y() << "," << w.Y() << std::endl;
    std::cout << v.Z() << "," << u.Z() << "," << w.Z() << std::endl;
    mrot*=fCuboid.GetTransformation().Rotation();
    mrot.GetComponents(v,u,w);
    std::cout << "Rotation: \n"; 
    std::cout << v.X() << "," << u.X() << "," << w.X() << std::endl;
    std::cout << v.Y() << "," << u.Y() << "," << w.Y() << std::endl;
    std::cout << v.Z() << "," << u.Z() << "," << w.Z() << std::endl;
    getchar();
    */

    // reference point
    TVector3 _local(local.X(), local.Y(), local.Z());
    utils::kGridPoint refPoint = this->GetClosestNeighbour(_local);
    //_local.Print();
    //std::get<0>(refPoint).Print();
    //getchar();
    
    TVector3 Idx = this->GetGridPointIndex(std::get<0>(refPoint));
    int i = Idx.X();
    int j = Idx.Y();
    int k = Idx.Z();

    double dX = fGrid.pXAxis.pStep;
    double dY = fGrid.pYAxis.pStep;
    double dZ = fGrid.pZAxis.pStep;

    // calculate field gradient
    double Bxx = 
      (std::get<1>(GetGridPointValue(TVector3(i-2,j,k))).X()
       -8.*std::get<1>(GetGridPointValue(TVector3(i-1,j,k))).X()
       +8.*std::get<1>(GetGridPointValue(TVector3(i+1,j,k))).X()
       -std::get<1>(GetGridPointValue(TVector3(i+2,j,k))).X())/(12.*dX);
    double Bxy = 
      (std::get<1>(GetGridPointValue(TVector3(i,j-2,k))).X()
       -8.*std::get<1>(GetGridPointValue(TVector3(i,j-1,k))).X()
       +8.*std::get<1>(GetGridPointValue(TVector3(i,j+1,k))).X()
       -std::get<1>(GetGridPointValue(TVector3(i,j+2,k))).X())/(12.*dY);
    double Bxz = 
      (std::get<1>(GetGridPointValue(TVector3(i,j,k-2))).X()
       -8.*std::get<1>(GetGridPointValue(TVector3(i,j,k-1))).X()
       +8.*std::get<1>(GetGridPointValue(TVector3(i,j,k+1))).X()
       -std::get<1>(GetGridPointValue(TVector3(i,j,k+2))).X())/(12.*dZ);
    double Byx = 
      (std::get<1>(GetGridPointValue(TVector3(i-2,j,k))).Y()
       -8.*std::get<1>(GetGridPointValue(TVector3(i-1,j,k))).Y()
       +8.*std::get<1>(GetGridPointValue(TVector3(i+1,j,k))).Y()
       -std::get<1>(GetGridPointValue(TVector3(i+2,j,k))).Y())/(12.*dX);
    double Byy = 
      (std::get<1>(GetGridPointValue(TVector3(i,j-2,k))).Y()
       -8.*std::get<1>(GetGridPointValue(TVector3(i,j-1,k))).Y()
       +8.*std::get<1>(GetGridPointValue(TVector3(i,j+1,k))).Y()
       -std::get<1>(GetGridPointValue(TVector3(i,j+2,k))).Y())/(12.*dY);
    double Byz = 
      (std::get<1>(GetGridPointValue(TVector3(i,j,k-2))).Y()
       -8.*std::get<1>(GetGridPointValue(TVector3(i,j,k-1))).Y()
       +8.*std::get<1>(GetGridPointValue(TVector3(i,j,k+1))).Y()
       -std::get<1>(GetGridPointValue(TVector3(i,j,k+2))).Y())/(12.*dZ);
    double Bzx = 
      (std::get<1>(GetGridPointValue(TVector3(i-2,j,k))).Z()
       -8.*std::get<1>(GetGridPointValue(TVector3(i-1,j,k))).Z()
       +8.*std::get<1>(GetGridPointValue(TVector3(i+1,j,k))).Z()
       -std::get<1>(GetGridPointValue(TVector3(i+2,j,k))).Z())/(12.*dX);
    double Bzy = 
      (std::get<1>(GetGridPointValue(TVector3(i,j-2,k))).Z()
       -8.*std::get<1>(GetGridPointValue(TVector3(i,j-1,k))).Z()
       +8.*std::get<1>(GetGridPointValue(TVector3(i,j+1,k))).Z()
       -std::get<1>(GetGridPointValue(TVector3(i,j+2,k))).Z())/(12.*dY);
    double Bzz = 
      (std::get<1>(GetGridPointValue(TVector3(i,j,k-2))).Z()
       -8.*std::get<1>(GetGridPointValue(TVector3(i,j,k-1))).Z()
       +8.*std::get<1>(GetGridPointValue(TVector3(i,j,k+1))).Z()
       -std::get<1>(GetGridPointValue(TVector3(i,j,k+2))).Z())/(12.*dZ);

    // interpolate
    TVector3 refField = std::get<1>(refPoint);
    TVector3 dir = _local-std::get<0>(refPoint);

    TVector3 newField;
    newField.SetX(refField.X()+(Bxx*dir.X()+Bxy*dir.Y()+Bxz*dir.Z()));
    newField.SetY(refField.Y()+(Byx*dir.X()+Byy*dir.Y()+Byz*dir.Z()));
    newField.SetZ(refField.Z()+(Bzx*dir.X()+Bzy*dir.Y()+Bzz*dir.Z()));

    //double mag = newField.Mag();
    //newField = newField.Unit();

    ROOT::Math::XYZPoint startPos( _local.X(), local.Y(), local.Z() );
    ROOT::Math::XYZPoint endPos( _local.X()+newField.X()
                               , _local.Y()+newField.Y()
                               , _local.Z()+newField.Z() );

    startPos = fCuboid.GetTransformation()*startPos;

    endPos = fCuboid.GetTransformation()*endPos;

    newField.SetXYZ( endPos.x()-startPos.x()
                   , endPos.y()-startPos.y()
                   , endPos.z()-startPos.z() );

    // TODO: correct for field rotation
    // ROOT::Math::Rotation3D rot = fCuboid.GetTransformation().Rotation();
    /*
    double rxx,rxy,rxz,ryx,ryy,ryz,rzx,rzy,rzz;
    rot.GetComponents(rxx,rxy,rxz,ryx,ryy,ryz,rzx,rzy,rzz);
    std::cout << rxx << " " << rxy << " " << rxz << std::endl;
    std::cout << ryx << " " << ryy << " " << ryz << std::endl;
    std::cout << rzx << " " << rzy << " " << rzz << std::endl;
    getchar();
    newField.SetX(newField.X()*rxx+newField.Y()*ryx+newField.Z()*rzx);
    newField.SetY(newField.X()*rxy+newField.Y()*ryy+newField.Z()*rzy);
    newField.SetZ(newField.X()*rxz+newField.Y()*ryz+newField.Z()*rzz);
    */
    //newField = rot*newField;

    return newField;

  }

} // end namespace utils
