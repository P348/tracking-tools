#include "NonUniformField.hh"
#include "MagneticFieldMap.hh"

// STD Library
#include <utility>
#include <iostream>
#include <stdlib.h>

namespace genfit{

  NonUniformField::NonUniformField(std::map<std::string, utils::MagneticFieldMap>& aMap,
      double FieldUnit,
      double PositionUnit)
    : fFieldMaps(aMap), fFieldUnit(FieldUnit), fPositionUnit(PositionUnit)
  {
    // ...
  }

  NonUniformField::~NonUniformField()
  {
    // ...
  }

  TVector3 NonUniformField::get(const TVector3& pos) const
  {
    TVector3 Field(0,0,0);
    TVector3 Pos = pos;
    Pos*=1./fPositionUnit;
    std::string MapName;

    // find map name
    std::map<std::string, utils::MagneticFieldMap>::const_iterator it;
    for (it = fFieldMaps.begin(); it != fFieldMaps.end(); ++it)
    {
      if (it->second.CheckRange(Pos)) {   
        TVector3 field = it->second.InterpolateFieldValue(Pos);
        Field = field*fFieldUnit;

        break;
      }
    }

#if 0
    // get the corresponding map
    if (MapName != "") {
      utils::MagneticFieldMap theMap;
      std::map<std::string, utils::MagneticFieldMap>::const_iterator it = fFieldMaps.find(MapName);
      if (it != fFieldMaps.end()) {
        theMap = it->second;
      }
      else {
        std::cerr << "Field map " << MapName << " not found, exiting." << std::endl;
        exit(EXIT_FAILURE);
      }
      TVector3 field = it->second.InterpolateFieldValue(Pos);
      Field = field*fFieldUnit;
    }
#endif
    return Field;
  }

  void NonUniformField::get(const double& x, const double& y, const double& z,
      double& Bx, double& By, double& Bz) const
  {
    Bx=0.;By=0.;Bz=0.;

    TVector3 Pos(x,y,z);
    Pos *= 1./fPositionUnit; // convert to mm
    std::string MapName;

    // find map name
    std::map<std::string, utils::MagneticFieldMap>::const_iterator it;
    for (it = fFieldMaps.begin(); it != fFieldMaps.end(); ++it)
    { 

      if (it->second.CheckRange(Pos)) {
        MapName = it->second.GetName();
        
        TVector3 field = it->second.InterpolateFieldValue(Pos);
        field *= fFieldUnit; // convert to kG
        Bx = field.X();
        By = field.Y();
        Bz = field.Z();
        //std::cout << MapName << ": (Bx,By,Bz)=(" << Bx << "," << By << "," << Bz <<")\n\n";

        break;
      }

    }
#if 0
    // get the corresponding map
    if (MapName != "") {
      utils::MagneticFieldMap theMap;

      std::map<std::string, utils::MagneticFieldMap>::const_iterator it = fFieldMaps.find(MapName);

      if (it != fFieldMaps.end()) {
        //theMap = it->second;
        TVector3 field = it->second.InterpolateFieldValue(Pos);
        field *= fFieldUnit; // convert to kG
        Bx = field.X();
        By = field.Y();
        Bz = field.Z();
      }

      else {
        std::cerr << "Field map " << MapName << " not found, exiting." << std::endl;
        exit(EXIT_FAILURE);
      }

    }
#endif
  }

  void NonUniformField::CheckMapsRange(const TVector3& Pos, std::string& MapName)
  {
    MapName = "";
    std::map<std::string, utils::MagneticFieldMap>::const_iterator it;
    for (it = fFieldMaps.begin(); it != fFieldMaps.end(); ++it)
    {
      if (it->second.CheckRange(Pos)) {
        MapName = it->second.GetName();
        break;
      }
    }
  }

  utils::MagneticFieldMap NonUniformField::GetMagneticFieldMap(std::string Name)
  {
    std::map<std::string, utils::MagneticFieldMap>::iterator it = fFieldMaps.find(Name);
    if (it != fFieldMaps.end()) {
      return it->second;
    }
    else {
      std::cerr << "Field map " << Name << " not found, exiting." << std::endl;
      exit(EXIT_FAILURE);
    }
  }

}
