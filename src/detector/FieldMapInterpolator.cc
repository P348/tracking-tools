// User
#include "FieldMapInterpolator.hh"

// STD Library
#include <stdexcept>
#include <algorithm>
#include <sstream>

void FieldMapInterpolator::ParseFieldFile( const std::string& fileName, const double& L, std::uint32_t nz, const double& scaling )
{
  // Open the input stream
  std::ifstream file( fileName, std::ios_base::in );
  if( !file.is_open() )
    throw std::runtime_error( "Error in <FieldMapInterpolator::ParseFieldFile()>: could not open file..." );

  // Parse the 2D field map
  std::vector<std::vector<Vector3>> fieldMap;

  std::string line;
  while (std::getline(file, line)) {
    std::stringstream ss(line);
    int i, j;
    double bx, by;
    char comma;
    ss >> i >> comma >> j >> comma >> bx >> comma >> by;

    if( fieldMap.size() <= static_cast<size_t>(i) ) {
      fieldMap.resize(i + 1);
    }
    if( fieldMap[i].size() <= static_cast<size_t>(j) ) {
      fieldMap[i].resize(j + 1);
    }
    fieldMap[i][j] = scaling*Vector3(bx, by, 0.0); // Bz is 0 for 2D map
  }

  file.close();

  // Set the bounds for x, y, and z
  m_bounds.m_xmin = 0.0;
  m_bounds.m_xmax = fieldMap.size() - 1; // Assuming grid starts at 0
  m_bounds.m_ymin = 0.0;
  m_bounds.m_ymax = fieldMap[0].size() - 1;
  m_bounds.m_zmin = -L;
  m_bounds.m_zmax = L;

  // Grid resolution
  m_bounds.m_nx = fieldMap.size();
  m_bounds.m_ny = fieldMap[0].size();
  m_bounds.m_nz = nz;

  m_bounds.m_dx = (m_bounds.m_xmax - m_bounds.m_xmin) / (m_bounds.m_nx - 1);
  m_bounds.m_dy = (m_bounds.m_ymax - m_bounds.m_ymin) / (m_bounds.m_ny - 1);
  m_bounds.m_dz = (m_bounds.m_zmax - m_bounds.m_zmin) / (m_bounds.m_nz - 1);

  // Extend the 2D field into 3D
  m_fieldData.resize(m_bounds.m_nx * m_bounds.m_ny * m_bounds.m_nz);
  for( std::uint32_t iz = 0; iz < m_bounds.m_nz; ++iz ) {
    for( std::uint32_t ix = 0; ix < m_bounds.m_nx; ++ix ) {
      for( std::uint32_t iy = 0; iy < m_bounds.m_ny; ++iy ) {
        Vector3 baseField = fieldMap[ix][iy];
        m_fieldData[Index(ix, iy, iz)] = baseField;
      }
    }
  }
}

const Vector3 FieldMapInterpolator::GetField( const Vector3& position ) const
{
  // Translate to the local coordinate system
  Vector3 localPos = m_rotation.transpose() * (position - m_translation);

  // Constant correction
  //localPos.x() += m_bounds.m_dx*0.5;
  //localPos.y() += m_bounds.m_dy*0.5;

  // Reflect the position into the first octant
  std::array< bool, 3 > flipAxis = {false,false,false};
  FirstOctantSymmetry(localPos, flipAxis);

  // Check bounds in the first octant
  if( localPos.x() < m_bounds.m_xmin || localPos.x() > m_bounds.m_xmax ||
      localPos.y() < m_bounds.m_ymin || localPos.y() > m_bounds.m_ymax ||
      localPos.z() < m_bounds.m_zmin || localPos.z() > m_bounds.m_zmax ) {
    return Vector3::Zero();
  }

  // Compute grid indices and fractional positions
  double fx = (localPos.x() - m_bounds.m_xmin) / m_bounds.m_dx;
  double fy = (localPos.y() - m_bounds.m_ymin) / m_bounds.m_dy;
  double fz = (localPos.z() - m_bounds.m_zmin) / m_bounds.m_dz;

  std::uint32_t ix = static_cast<std::uint32_t>(fx);
  std::uint32_t iy = static_cast<std::uint32_t>(fy);
  std::uint32_t iz = static_cast<std::uint32_t>(fz);

  double dx = fx - ix;
  double dy = fy - iy;
  double dz = fz - iz;

  // Fetch field values at the surrounding grid points
  auto getFieldAt = [&]( 
      std::uint32_t x, std::uint32_t y, std::uint32_t z
      ) -> Vector3 {
    return m_fieldData.at(Index( std::clamp(x, 0u, m_bounds.m_nx - 1u)
                               , std::clamp(y, 0u, m_bounds.m_ny - 1u)
                               , std::clamp(z, 0u, m_bounds.m_nz - 1u))
        );
  };

  Vector3 f000 = getFieldAt(ix, iy, iz);
  Vector3 f100 = getFieldAt(ix + 1, iy, iz);
  Vector3 f010 = getFieldAt(ix, iy + 1, iz);
  Vector3 f001 = getFieldAt(ix, iy, iz + 1);
  Vector3 f110 = getFieldAt(ix + 1, iy + 1, iz);
  Vector3 f101 = getFieldAt(ix + 1, iy, iz + 1);
  Vector3 f011 = getFieldAt(ix, iy + 1, iz + 1);
  Vector3 f111 = getFieldAt(ix + 1, iy + 1, iz + 1);

  // Perform trilinear interpolation
  Vector3 interpolatedField =
    (1 - dx) * (1 - dy) * (1 - dz) * f000 +
    dx * (1 - dy) * (1 - dz) * f100 +
    (1 - dx) * dy * (1 - dz) * f010 +
    (1 - dx) * (1 - dy) * dz * f001 +
    dx * dy * (1 - dz) * f110 +
    dx * (1 - dy) * dz * f101 +
    (1 - dx) * dy * dz * f011 +
    dx * dy * dz * f111;

  // Apply symmetry transformations based on the original octant
  if( flipAxis[0] ) interpolatedField.x() = -interpolatedField.x();
  if( flipAxis[1] ) interpolatedField.x() = -interpolatedField.x();

  return m_rotation*interpolatedField;

}
