#include "MagnetMBPL.hh"

namespace Magnet{

  MBPL::MBPL(std::string DetectorName) : Generic(DetectorName)
  {
    // ...
  }

  MBPL::~MBPL()
  {
    // ...
  }

  void MBPL::SetMBPLDimensions(Magnet::MBPLDimensions Dimensions)
  {
    fDimensions = Dimensions;
  }

  MBPLDimensions MBPL::GetMBPLDimensions()
  {
    return fDimensions;
  }


} // end namespace Magnet
