#include "IOStreamBuffer.hh"
#include <iostream>
#include <sstream>
#include <fstream>
#include <cstdio>


IOStreamBuffer::IOStreamBuffer() : _logfile("log.txt", std::ofstream::app) {
  _logfilename = "log.txt";
  _save2Log = true;
  _had_output = false;
  std::ostringstream oss;
  oss << std::endl;
  _pdNl = oss.str();
}

IOStreamBuffer::IOStreamBuffer(std::string logfilename) : _logfile(logfilename, std::ofstream::app) {
  _logfilename = logfilename;
  _save2Log = true;
  _had_output = false;
  std::ostringstream oss;
  oss << std::endl;
  _pdNl = oss.str();
}

IOStreamBuffer::~IOStreamBuffer() {
  if (!_had_output) {
    std::remove(_logfilename.c_str());
  }
}

void IOStreamBuffer::SetSaveLog(bool saveLog) {
  _save2Log = saveLog;
}

void IOStreamBuffer::_print_message_buffer() {
  if (!_logfile.is_open()) {
    //std::cerr << "IOStreamBuffer Error: Unable to open output file." << std::endl;
    return;
  }
  if (!_save2Log) return;

  for( size_t n = _buf.find(_pdNl) ; n != std::string::npos && !_buf.empty() ; n = _buf.find(_pdNl)) {
    if(_buf == _pdNl) {
      _buf.clear();
      break;
    }
    _logfile << _buf.substr(0, n);
    _buf = _buf.substr(n+_pdNl.size());
  }
}

std::streamsize IOStreamBuffer::xsputn(const char_type * c, std::streamsize n) {
  if(0 == n) return 0;
  std::string part(c, c+n);
  _buf += part;
  _print_message_buffer();
  return n;
}

std::streambuf::int_type IOStreamBuffer::overflow(int_type c) {
  if(!traits_type::eq_int_type(c, traits_type::eof())){
    char_type const t = traits_type::to_char_type(c);
    this->xsputn(&t, 1);
    this->_had_output = true;
  }
  return !traits_type::eof();
}

int IOStreamBuffer::sync() {
  if(_buf.empty()) return 0;
  _print_message_buffer();
  return 0;
}


