#include "IOStreamManager.hh"

// STD library
#include <iostream>
#include <stdlib.h>

// ROOT
#include "TTree.h"


IOStreamManager* IOStreamManager::fMgr = nullptr;

IOStreamManager::IOStreamManager()
{
  fInputTree = nullptr;
  fOutputTree = nullptr;
  fInputList = nullptr;
}

IOStreamManager::~IOStreamManager()
{
  // ...
}

IOStreamManager* IOStreamManager::GetInstance()
{
  if (!fMgr)
    fMgr = new IOStreamManager();
  return fMgr;
}

TTree* IOStreamManager::GetInputTree()
{
  return fInputTree;
}

TTree* IOStreamManager::GetOutputTree()
{
  return fOutputTree;
}

TList* IOStreamManager::GetInputParam()
{
  return fInputList;
}

void IOStreamManager::Close()
{
  // ...
}

