#include "ConfigFileParser.hh"

// STD Library
#include <iostream>
#include <stdlib.h>
#include <iomanip>
#include <typeinfo>

// Boost
#include <boost/property_tree/json_parser.hpp>
#include <boost/optional.hpp>
#include <boost/foreach.hpp>
#include <boost/throw_exception.hpp>

ConfigFileParser* ConfigFileParser::fParser = nullptr;

void PrintValues(const std::string& key, const boost::property_tree::ptree& tree, bool shift = false)
{
  std::string tab = shift ? "\t" : "";

  // "key : entry"
  if (tree.empty()) 
    std::cout << tab << std::setw(20) << std::left << std::setfill('.') << key << ": " 
      << std::right << tree.data() << std::endl;  
  // "key : array"
  else if (tree.size() > 0 && tree.begin()->first == "") {
    std::cout << tab << std::setw(20) << std::left << std::setfill('.') << key << ": "; 
    for (auto it = tree.begin(); it != tree.end(); it++)
    {
      if (std::distance(tree.begin(), it) < static_cast<int>(tree.size()-1)) 
        std::cout << std::right << it->second.data() << ", ";
      else break;
    }
    std::cout << std::right << tree.rbegin()->second.data() << std::endl;
  }
}

ConfigFileParser::ConfigFileParser()
{
  // ...
}

ConfigFileParser::ConfigFileParser(std::string ConfigFile)
{
  fConfigFile = ConfigFile;
  boost::property_tree::read_json(fConfigFile, fPtree);
}

ConfigFileParser::~ConfigFileParser()
{
  // ...
}

ConfigFileParser* ConfigFileParser::GetConfigFileParser()
{
  if (!fParser)
    fParser = new ConfigFileParser();
  return fParser;
}

void ConfigFileParser::SetConfigFile(std::string ConfigFile)
{
  fConfigFile = ConfigFile;
  boost::property_tree::read_json(fConfigFile, fPtree);
}

void ConfigFileParser::PrintConfigFile()
{
  // NOTE: only supports one sub-tree for now.

  std::cout << "Configuration file used \033[1;33m" << fConfigFile 
    << "\033[0m with inputs:" << std::endl;
  BOOST_FOREACH(boost::property_tree::ptree::value_type& value, fPtree) {
    const std::string& key = value.first;
    const boost::property_tree::ptree& tree = value.second;

    // comment
    if (key.find("//") != std::string::npos) continue;

    PrintValues(key, tree);
    // "key : sub-tree"
    if (tree.size() > 0 && tree.begin()->first != "") {
      std::cout << std::setw(20) << std::left << std::setfill('.') << key << ". \n"; 
      for (auto it = tree.begin(); it != tree.end(); it++)
      {
        const std::string& subkey = it->first;
        const boost::property_tree::ptree& subtree = it->second;

        // comment
        if (subkey.find("//") != std::string::npos) continue;
        PrintValues(subkey, subtree, false);
      }
    }
  }

}

bool ConfigFileParser::GetConfigValueWithABool(std::string name)
{
  if (boost::optional<bool> value = fPtree.get_optional<bool>(name)) {
    return *value;
  }
  else {
    std::cerr << name << " does not referred to an <bool>." << std::endl;
    std::cerr << "Note: " << name << " possibly not (correctly) assigned." << std::endl;
    exit(EXIT_FAILURE);
  }

}

int ConfigFileParser::GetConfigValueWithAnInt(std::string name)
{
  if (boost::optional<int> value = fPtree.get_optional<int>(name)) {
    return *value;
  }
  else {
    std::cerr << name << " does not referred to an <int>." << std::endl;
    std::cerr << "Note: " << name << " possibly not (correctly) assigned." << std::endl;
    exit(EXIT_FAILURE);
  }
}

double ConfigFileParser::GetConfigValueWithADouble(std::string name)
{
  if (boost::optional<double> value = fPtree.get_optional<double>(name)) {
    return *value;
  }
  else {
    std::cerr << name << "  does not referred to a <double>." << std::endl;
    std::cerr << "Note: " << name << " possibly not (correctly) assigned." << std::endl;
    exit(EXIT_FAILURE);
  }
}

std::string ConfigFileParser::GetConfigValueWithAString(std::string name)
{
  if (boost::optional<std::string> value = fPtree.get_optional<std::string>(name)) {
    return *value;
  }
  else {
    std::cerr << name << "  does not referred to an <std::string>." << std::endl;
    std::cerr << "Note: " << name << " possibly not (correctly) assigned." << std::endl;
    exit(EXIT_FAILURE);
  }
}

TVector2 ConfigFileParser::GetConfigValueWithATVector2(std::string name)
{
  std::vector<double> values;
  for (boost::property_tree::ptree::value_type& val : fPtree.get_child(name)) 
  {
    values.push_back(val.second.get_value<double>());
  }

  return TVector2(values.at(0), values.at(1));
}

TVector3 ConfigFileParser::GetConfigValueWithATVector3(std::string name)
{
  std::vector<double> values;
  for (boost::property_tree::ptree::value_type& val : fPtree.get_child(name)) 
  {
    values.push_back(val.second.get_value<double>());
  }

  return TVector3(values.at(0), values.at(1), values.at(2));
}

bool ConfigFileParser::GetConfigValueFromListWithABool(std::string name, 
    std::string subname)
{
  boost::property_tree::ptree tree; 
  try {
    bool value = false;
    for (boost::property_tree::ptree::value_type& val : fPtree.get_child(name)) 
    {
      // get the label
      if (val.first == subname) {
        value = val.second.get_value<bool>();
        break;
      }
    }
    return value;
  }
  //catch (const boost::wrapexcept<boost::property_tree::ptree_bad_path>& e) {  
  catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    std::cerr << subname << " does not correspond to a node of the tree." << std::endl;
    exit(EXIT_FAILURE);
  }
}

int ConfigFileParser::GetConfigValueFromListWithAnInt(std::string name, 
    std::string subname)
{
  boost::property_tree::ptree tree; 
  try {
    int value = 0;
    for (boost::property_tree::ptree::value_type& val : fPtree.get_child(name)) 
    {
      // get the label
      if (val.first == subname) {
        value = val.second.get_value<int>();
        break;
      }
    }
    return value;
  }
  //catch (const boost::wrapexcept<boost::property_tree::ptree_bad_path>& e) {  
  catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    std::cerr << subname << " does not correspond to a node of the tree." << std::endl;
    exit(EXIT_FAILURE);
  }
}

double ConfigFileParser::GetConfigValueFromListWithADouble(std::string name, 
    std::string subname)
{
  boost::property_tree::ptree tree; 
  try {
    double value = 0.0;
    for (boost::property_tree::ptree::value_type& val : fPtree.get_child(name)) 
    {
      // get the label
      if (val.first == subname) {
        value = val.second.get_value<double>();
        break;
      }
    }
    return value;
  }
  //catch (const boost::wrapexcept<boost::property_tree::ptree_bad_path>& e) {  
  catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    std::cerr << subname << " does not correspond to a node of the tree." << std::endl;
    exit(EXIT_FAILURE);
  }
}

std::string ConfigFileParser::GetConfigValueFromListWithAString(std::string name,
    std::string subname)
{

  boost::property_tree::ptree tree; 
  try {
    std::string value = "";
    for (boost::property_tree::ptree::value_type& val : fPtree.get_child(name)) 
    {
      // get the label
      if (val.first == subname) {
        value = val.second.get_value<std::string>();
        break;
      }
    }
    return value;
  }
  //catch (const boost::wrapexcept<boost::property_tree::ptree_bad_path>& e) {  
  catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    std::cerr << subname << " does not correspond to a node of the tree." << std::endl;
    exit(EXIT_FAILURE);
  }

}

TVector2 ConfigFileParser::GetConfigValueFromListWithATVector2(std::string name,
    std::string subname)
{

  boost::property_tree::ptree tree; 
  try {
    tree = fPtree.get_child(name);
    const boost::property_tree::ptree& subtree = tree.get_child(subname);

    std::vector<double> values;
    for (auto it = subtree.begin(); it != subtree.end(); it++) 
      values.push_back(it->second.get_value<double>());

    return TVector2(values.at(0), values.at(1));
  }
  //catch (const boost::wrapexcept<boost::property_tree::ptree_bad_path>& e) {  
  catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    std::cerr << subname << " does not correspond to a node of the tree." << std::endl;
    exit(EXIT_FAILURE);
  }

}

TVector3 ConfigFileParser::GetConfigValueFromListWithATVector3(std::string name,
    std::string subname)
{

  boost::property_tree::ptree tree; 
  try {
    tree = fPtree.get_child(name);
    const boost::property_tree::ptree& subtree = tree.get_child(subname);

    std::vector<double> values;
    for (auto it = subtree.begin(); it != subtree.end(); it++) 
      values.push_back(it->second.get_value<double>());

    return TVector3(values.at(0), values.at(1), values.at(2));
  }
  //catch (const boost::wrapexcept<boost::property_tree::ptree_bad_path>& e) {  
  catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    std::cerr << subname << " does not correspond to a node of the tree." << std::endl;
    exit(EXIT_FAILURE);
  }

}
