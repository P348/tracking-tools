#!/bin/bash
# Default environment setting, can be modified by the user to account for its
# build version.

# LCG setup
source /cvmfs/sft.cern.ch/lcg/views/setupViews.sh LCG_96b x86_64-centos7-gcc62-opt

echo -e "\033[1;32mConfiguring...\033[0m"
export TT_DIR="/afs/cern.ch/work/h/hsieber/work/dev/na64-muon/opt/tracking-tools"
export GF_DIR="/afs/cern.ch/work/h/hsieber/work/dev/na64-muon/opt/GenFit"
export BOOST_DIR="/cvmfs/sft.cern.ch/lcg/releases/LCG_96b/Boost/1.70.0/x86_64-centos7-gcc62-opt"
export ROOT_DIR="/cvmfs/sft.cern.ch/lcg/releases/ROOT/6.18.04-c767d/x86_64-centos7-gcc62-opt"

echo -e "\033[0;31mUsing Tracking Tools with location ${TT_DIR}\033[0m"
echo -e "\033[0;31mUsing GenFit with location ${GF_DIR}\033[0m"
echo -e "\033[0;31mUsing ROOT with location ${ROOT_DIR}\033[0m"
echo -e "\033[0;31mUsing Boost with location ${BOOST_DIR}\033[0m"
