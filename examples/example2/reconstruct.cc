/*! \file reconstruct.cc
    \author Henri Hugo Sieber <henri.hugo.sieber@cern.ch>
    \brief Main track reconstruction file
*/

// STD Library
#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <fstream>

// ROOT
#include <TH1F.h>
#include <TRandom.h>
#include <TFile.h>
#include <TDirectory.h>

// Tracking tools
#include <ConfigFileParser.hh>

/*! \brief Load the detector plane map
    \param name The name of the file
    \param verboseLvl The level of verbose
*/
std::map<std::string, int>
LoadPlaneMap( std::string name, int verboseLvl ) 
{

  if( verboseLvl >= 1 )
    std::cout << "\033[1;36mInitialising Millepede labelling...\033[0m" << std::endl;

  // Define the IO stream
  std::ifstream inFile(name, std::ios_base::in);

  std::map<std::string, int> aMap;
  std::string aName;
  int aPlaneId;
  double isFixed;

  // Parse the input file
  std::string str;
  inFile.ignore ( std::numeric_limits<std::streamsize>::max(), '\n' );
  while( std::getline( inFile, str ) ) {
    std::istringstream iss(str);
    std::cout << str << std::endl;
    if( str.find("#") != std::string::npos ) continue;
    if( str.find("!") != std::string::npos ) continue;
    iss >> aName >> aPlaneId >> isFixed;
    aMap.insert( {aName, aPlaneId} );
  }

  return aMap;
}

/*! \brief main function. */
int main( int argc, const char* argv[])
{

  // Init the configuration files

  /*! The main configuration file. */
  ConfigFileParser config("./resources/config.json");
  unsigned int verboseLvl     = config.GetConfigValueFromListWithAnInt   ("Parameters", "Verbose level");
  std::string outputFile      = config.GetConfigValueFromListWithAString ("Files", "ROOT results");
  std::string trackersFile    = config.GetConfigValueFromListWithAString ("Files", "Detectors map");
  std::string placementsFile  = config.GetConfigValueFromListWithAString ("Files", "Placements");
  std::string correctionsFile = config.GetConfigValueFromListWithAString ("Files", "Corrections");

  /*! The placement configuration file. */
  ConfigFileParser placements(placementsFile);

  /*! The correction configuration file. */
  ConfigFileParser corrections(correctionsFile);

  // Input / output stream for ROOT files
  // NOTE: you would here define your input file with the detector hits
  // to reconstruct the track, etc... Typically that would be some ROOT file produced
  // with p348reco where one would have the distributions of tracking detectors X and Y 
  // coordinates.

  /*! The input file */
  // ...

  /*! The input TTree */
  // TTree* aTree = static_cast<TTree*>( ... )

  /*! The output file */
  TFile* outFile = new TFile(outputFile.c_str(), "recreate");

  // Define the residuals histograms
  TDirectory* residualsDir = outFile->mkdir("residuals");
  std::map<std::string, TH1F*> planarResiduals;

  // Init the detector planes
  // NOTE: the labelling of the detector planes is expected to be the following
  // - Micromegas: MM<n><plane> where n = 1,2,3,4,... is the Micromegas ID and plane = x, y. One would have
  //   typically for Micromega 4 x plane, MM4x
  // - GEMs: GEM<n><plane>
  // - Straws: ST0<n>_<m><plane> where n = 1,2,3,4,... is the Straw ID, m = 1,2,3,4 is the plane ID and plane = x,y
  //   corresponds to hits along the x or y coordinate. In practice one would only have ST01_1x, ST01_2x, ST01_3y and
  //   ST01_4y.
  // - BMS: BMS<n>y since we only have y coordinate for BMS
  const double rangeUp  = config.GetConfigValueFromListWithADouble("Parameters", "Residuals upper range");
  const double rangeLow = config.GetConfigValueFromListWithADouble("Parameters", "Residuals lower range");
  std::map<std::string, int> detPlanes = LoadPlaneMap( trackersFile, verboseLvl );

  std::string str;
  for( auto it = detPlanes.begin(); it != detPlanes.end(); it++ ) {
    if( it->first.find("MM") != std::string::npos || it->first.find("GEM") != std::string::npos ) { // Micromegas and GEMs
      if( it->first.find("X") != std::string::npos ) {
        str = it->first.substr(0, it->first.length()-1);
        planarResiduals.insert( { str+"x_ures"
                              , new TH1F((str+"x_ures").c_str(), "", 200, rangeLow, rangeUp)} );
      }
      else if( it->first.find("Y") != std::string::npos ) {
        str = it->first.substr(0, it->first.length()-1);
        planarResiduals.insert( { str+"y_ures"
                              , new TH1F((str+"y_ures").c_str(), "", 200, rangeLow, rangeUp)} );
      }
    }
    else if( it->first.find("ST") != std::string::npos ) { // Straws
      planarResiduals.insert( { (it->first+"_ures").c_str() 
                            , new TH1F((it->first+"_ures").c_str(), "", 200, rangeLow, rangeUp)} );
    }
    else if( it->first.find("BMS") != std::string::npos ) { // BMS
      str = it->first.substr(0, it->first.length()-1);
      planarResiduals.insert( { str + "y_ures"
                            , new TH1F((str+"y_ures").c_str(), "", 200, rangeLow, rangeUp)} );
    }
  }

  // Process with the track reconstruction
  /*

      Track reconstruction routine here ...

      // NOTE: the hits are expected to be in the local coordinate system of the tracking detector. One
      // can use the placements to retrieve the global coordinates, e.g.
      //  
      //  placementsFile.GetConfigValueFromListWithATVector3("MM2X", "origin").X()
      //
      // ...
      //
      // Also the corrections should be used here
      //
      //  correctionsFile.GetConfigValueFromListWithATVector2("MM2X", "correction").X()
      //
      // ...

  */

  // Save the residuals
  // The residuals can be extracted from e.g. your GenFit track reconstruction. Typically one could use the code
  // snippet below.
  /*
  if (fitTrack->getFitStatus()->isFitConverged()) {
    for (unsigned int i=0; i<fitTrack->getNumPoints(); i++) {
      genfit::TrackPoint* tp = fitTrack->getPointWithMeasurement(i);

      if( tp->hasRawMeasurements() && tp->hasFitterInfo(fitTrack->getCardinalRep())) {
        TVectorD rawPoint = tp->getRawMeasurement(0)->getRawHitCoords();
        // tp->Print();
        TVectorD res_state_unbiased
          = tp->getFitterInfo(fitTrack->getCardinalRep())->getResidual().getState();

        TVector3 planeO 
          = tp->getFitterInfo(fitTrack->getCardinalRep())->getPlane()->getO();

        // Identify the corresponding detector plane name with e.g. its true Z position and
        // compare with the planeO Z position. Then fill the planarResiduals histograms
        // accordingly.
      }
    }
  }
  */

  // NOTE: in this example we assume some randomly generate residuals
  for( unsigned int r = 0; r < 1e6; r++ ) {
    planarResiduals["MM1x_ures"]->Fill(gRandom->Gaus(0.7, 0.1));
    planarResiduals["MM1y_ures"]->Fill(gRandom->Gaus(-0.45, 0.1));
  }

  // Write and close the I/O stream
  for( auto it = planarResiduals.begin(); it != planarResiduals.end(); it++ )
    residualsDir->WriteObjectAny( it->second, "TH1F", it->second->GetName() );
  outFile->Close();

  if( verboseLvl >= 1) 
    std::cout << "\033[1;36mFinishing reconstruction...\033[0m" << std::endl;

  return 0;

}
