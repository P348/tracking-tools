# Example 1
This example gives an example skeleton for a track-based alignment program (only in the directions perpendicular to the track trajectory, namely x and y). The repository contains two executbales to be compiled (`reconstruct.cc` and `corrections.cc`).

## Building the example
The environment for building the executable can be set in the `configure.sh` bash script. Then the source files (`.cc`) are built through the command
```sh
$ source build.sh
```

## The `reconstruct.cc` file
This file is meant to reconstruct tracks from a given input file with hits (e.g. output from `p348reco`) and encompass the given data placements and the computed corrections. It also produces the residuals and save them to histograms. In this example, no track is reconstructed but dummy residuals are produced as an example. The configuration for this executable can be found in `./resources/config.json`. The file is ran through
```sh
$ ./reconstruct.exe
```

## The `corrections.cc` file
This file is meant to compute the corrections to applied to the placements following a track-based alignment approach. To do so, one has to define pivot detectors and which ones to align. The list of tracking detector is defined in the file `./resources/map-data.txt`. This nomenclature is `name`, `label` and `fixed`. The `name` is the name of the detector plane, the `label` is its unique ID and the `fixed` parameter is -1.0 to not move the detector in the alignment and 0.0 to move it. The executable assumes a gaussian distribution of the residuals. The file is ran through
```sh
$ ./corrections.exe
```
which assumes as an input the ROOT file produced through the `reconstruct.exe` program.

## Visualising the residuals
The residuals can be automatically plotted through the ROOT macro `drawResiduals.C`.
