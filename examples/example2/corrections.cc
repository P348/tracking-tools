/*! \file corrections.cc 
    \author Henri Hugo Sieber <henri.hugo.sieber@cern.ch>
    \brief Track-based corrections estimator
*/

// STD Library
#include <iostream>
#include <fstream>
#include <map>
// #include <filesystem>
#include <cstdlib>
#include <exception>

// ROOT
#include <TSystem.h>
#include <TApplication.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TClass.h>
#include <TKey.h>
#include <TString.h>
#include <TStyle.h>
#include <TF1.h>
#include <TFitResult.h>
#include <TFitResultPtr.h>
#include <TSpectrum.h>
#include <TFile.h>
#include <TCanvas.h>

// Boost
#include <boost/algorithm/string/find.hpp>
#include <boost/range/iterator_range.hpp>
#include <boost/filesystem.hpp>

// Tracking Tools
#include <ConfigFileParser.hh>

/*! \class TEST_EXCEPTION
    \brief User-defined expression 

    std::exception derived class for personalize error message in error
    handling scheme.
*/
class TEST_EXCEPTION : public std::exception
{
  public:
    /*! \brief The constructor */
    TEST_EXCEPTION(const std::string& str) 
      : msg(std::string("Adding new corrections for plane "+str))
    {}
    /*! \brief Explanatory information string */
    virtual const char* what() const throw() { return msg.c_str(); }

  private:
    /*! The error message to be output. */
    std::string msg; 
};

/*! \brief Load the plane configuration
    
    Load the plane configuration into a map to get the label
    corresponding to the detector planes and their activation 
    values. A typical file is plane name, label, is active.

    \param name The name of the input configuration file.
    \param verboseLvl The level of verbose for run-time.
*/
std::map<std::string, std::pair<int, double> >
LoadPlaneConfiguration( std::string name
                      , int verboseLvl)
{
  if( verboseLvl >= 1 )
    std::cout << "\033[1;36mInitialising Millepede labelling...\033[0m" << std::endl;

  // The IO stream definition
  std::ifstream inFile(name, std::ios_base::in);

  std::map<std::string, std::pair<int, double> > aMap;
  std::string aName;
  std::string comment;
  int aPlaneId;
  double isFixed;

  // Parse the input file
  std::string str;
  inFile.ignore ( std::numeric_limits<std::streamsize>::max(), '\n' );
  while( std::getline( inFile, str ) ) {
    std::istringstream iss(str);
    if( verboseLvl >= 1 ) std::cout << str << std::endl;
    if( str.find("#") != std::string::npos ) continue; // general comment
    if( str.find("!") != std::string::npos ) { // comments begin with "!", those planes are unused
      iss >> comment >> aName >> aPlaneId >> isFixed;
      isFixed = -2.0;
    }
    else iss >> aName >> aPlaneId >> isFixed;
    aMap.insert( {aName, std::make_pair(aPlaneId, isFixed) } );
  }

  return aMap;
}

/*! \brief Extract substring from JSON file
    
    Extract substring in between quotes from the JSON input file
    \param str The input string
*/
std::string 
ExtractFromJSONQuotes( std::string str ) 
{
  // the name is expected to be in between quotes
  boost::iterator_range<std::string::iterator> it1 = boost::find_nth(str, "\"", 0);
  boost::iterator_range<std::string::iterator> it2 = boost::find_nth(str, "\"", 1);

  unsigned int start = std::distance(str.begin(), it1.begin());
  unsigned int end   = std::distance(str.begin(), it2.begin());

  return str.substr(start+1, end-start-1);
}

/*! \brief The alignment parameters estimator.

    Estimate the track-based alignment based on a set of histograms of 
    unbiased residuals obtained through a track reconstruction program. This 
    typically comes from e.g. GenFit. The histograms are expected to
    be saved in a ROOT file inheriting from the TH1 class.

    \param argc
    \param argv[]
*/
int
main( int argc, char* argv[] )
{
  // Define the input configuration file
  ConfigFileParser config("./resources/config.json");
  std::string folder = "./resources/corrections/";

  std::map<std::string, std::pair<int,double> > detPlanes
    = LoadPlaneConfiguration( config.GetConfigValueFromListWithAString("Files","Detectors map")
                            , 1);

  bool _display 
    = config.GetConfigValueFromListWithABool("Parameters", "Display residuals");

  // For event display using TCanvas
  TApplication* gMyRootApp;
  gMyRootApp = new TApplication("My ROOT Application", &argc, argv);

  // The map of corrections
  std::map<std::string, std::pair<double,double> > correctionsMap;

  // Define the IO stream for the ROOT file
  TFile* inFile 
    = TFile::Open( config.GetConfigValueFromListWithAString( "Files", "ROOT results").c_str()
                 , "READ");

  // Parameters
  double widthScan = config.GetConfigValueFromListWithADouble("Parameters", "Residuals width");

  if( !inFile ) return 1;

  // Gather the histograms
  std::vector<TH1F*> residuals;
  for( TObject* keyAsObj : *inFile->GetListOfKeys() ) {
    auto key = dynamic_cast<TKey*>(keyAsObj);
    if( key->GetName() == (TString)"residuals" ) {
      TDirectory* dir = dynamic_cast<TDirectory*>(key->ReadObj());
      TIter keyList(dir->GetListOfKeys());
      TKey* _key;
      while( (_key = (TKey*)keyList()) ) {
        TClass *cl = gROOT->GetClass(_key->GetClassName());
        if (!cl->InheritsFrom("TH1")) continue;
        TH1F *h = (TH1F*)_key->ReadObj();
        
        // TODO: fix this in IO stream of test executable ?
        if( h->GetName() != (TString)"momPull" )residuals.push_back(h);
      }
    }
  }

  // Prepare the canvas
  Int_t size = std::ceil(sqrt(residuals.size()));
  TCanvas* canvas = new TCanvas("c","c",2800,1800);
  canvas->Divide(size, size);

  // Fit and draw histograms
  TH1F* hist = nullptr;
  TF1* fitFunc = nullptr;
  TFitResultPtr ptr = nullptr;
  std::string str = "";
  bool kSpectrumFit = true;
  for( unsigned int i = 0; i < residuals.size(); i++ ) {
    canvas->cd(i+1);

    ptr = nullptr;

    // draw
    hist = residuals.at(i);
    if( !hist ) continue;
    hist->Draw("hist");

    // fit the histograms
    str = hist->GetName();
    if( !kSpectrumFit ) { // fitting the peak of the gaussian distribution

      fitFunc = new TF1((str+"_fitfunc").c_str(), "gaus", -10.0, 10.0);
      ptr = hist->Fit((str+"_fitfunc").c_str(), "RS");
      fitFunc->Draw("same");
    }
    else { // fitting the found peak for non gaussian distribution
      
      const int npeaks = 1;
      const double sigma = 10.; // 2, 5, 10
      const double threshold = 0.1;

      TSpectrum* spk = new TSpectrum(npeaks);
      int nfound = spk->Search(hist,sigma,"", threshold);

      // loop over peaks
      if (nfound > 0) {
        std::cout << "Info in <TSpectrum::Search>: Found " << nfound << " peaks !\n";

        double* xpeaks = spk->GetPositionX(); 

        for (UInt_t n = 0; n < nfound; n++) {
          double pos = xpeaks[n];
          int bin = hist->GetXaxis()->FindBin(pos);
          double amp = hist->GetBinContent(bin);

          // fit
          fitFunc 
            = new TF1((str+"_fitfunc"+std::to_string(n)).c_str(), "gaus", pos-widthScan, pos+widthScan);
          fitFunc->SetParameters(amp,pos,10.);
          ptr = hist->Fit((str+"_fitfunc"+std::to_string(n)).c_str(), "RS");
          fitFunc->Draw("same");
        }
      }

    }

    gStyle->SetOptFit(true);

    // gather the results
    double mean = 0.0;
    if( ptr != -1 ) mean = ptr->Parameter(1);

    // fill in the results in the map
    if( str.find("ST") == std::string::npos ) { // nomenclature is fixed by tracking
      bool isPlaneX = (str.find("x") != std::string::npos ) ? true : false;
      str = str.substr(0, str.length()-6);
      str = isPlaneX ? str+"X" : str+"Y";
    }
    else {
      str = str.substr(0, str.length()-5);
    }
    correctionsMap.insert( {str, std::make_pair(mean, std::get<1>(detPlanes[str])) } );

  }

  // fill the remaining of the unused planes
  for( auto it = detPlanes.begin(); it != detPlanes.end(); it++ ) {
    if( std::get<1>(it->second) == -2.0 ) {
      correctionsMap.insert( {it->first, std::make_pair(0.0, -1.0)} );
    }
  }

  // write the JSON corrections file
  // Note: about nomenclature.
  // - The latest corrections file is always of the form corrections<0,1>.json
  // - Previous versions of the files are of the form corrections<0,1>-v<version>.json
  unsigned int id = 0;
  std::string outName = folder+"corrections"+std::to_string(id)+".json";
  bool exists = false;
#if 0
  for( auto& File : std::__fs::filesystem::directory_iterator(folder) ) {
    str = File.path().string();
    std::cout << str << std::endl;
    if( str == outName ) exists = true;
  }
#endif
  boost::filesystem::path pFolder = folder;
  for(auto& File : boost::make_iterator_range(boost::filesystem::directory_iterator(pFolder), {})) {
    str = File.path().string();
    std::cout << str << std::endl;
    if( str == outName ) exists = true;
  }
  
  // new file
  if( ! exists ) {
    std::cout << "Info in <main>: writting new corrections file.\n";
    std::ofstream outFile(outName, std::ios_base::out);
    TVector2 corr(0.0,0.0);
    bool isEOF = false;
    std::vector<double> fixed;

    outFile << "{\n";
    for( auto it = correctionsMap.begin(); it != correctionsMap.end();++it) {

      if( std::distance(it, correctionsMap.end() ) == 1) isEOF = true;
      if( std::get<1>(it->second) == -1.0 ) {
        corr.Set(0.0,0.0);
        outFile << "\t\"" << it->first << "\": {\n";
        outFile << "\t\t\"correction\" : [" << corr.X() << ", " << corr.Y() << "]\n";
        if( !isEOF ) outFile << "\t},\n";
        else outFile << "\t}\n";
      }
      if( std::get<1>(it->second) != -1.0 ) { // add correction
        outFile << "\t\"" << it->first << "\": {\n";
        corr.Set(0.0,0.0);
        if(  (it->first.find("X") != std::string::npos) 
          ||(it->first.find("_1") != std::string::npos)
          ||(it->first.find("_2") != std::string::npos) ) { 
          corr.Set(std::get<0>(it->second), 0.0);
        }
        else corr.Set(0.0,std::get<0>(it->second));
        outFile << "\t\t\"correction\" : [" << corr.X() << ", " << corr.Y() << "]\n";
        if( !isEOF ) outFile << "\t},\n";
        else outFile << "\t}\n";
      }
    }
    outFile << "}";

    outFile.close();
  }
  else { // updating current configuration file
    std::cout << "Info in <main>: updating corrections file.\n";

    // find latest version
    std::vector<std::string> versions;
#if 0
    for( auto& File : std::__fs::filesystem::directory_iterator(folder) ) {
      str = File.path().string();
      if( str.find("corrections"+std::to_string(id)) != std::string::npos ) {
        versions.push_back(str);
      }
    }
#endif
    for(auto& File : boost::make_iterator_range(boost::filesystem::directory_iterator(pFolder), {})) {
      str = File.path().string();
      if( str.find("corrections"+std::to_string(id)) != std::string::npos ) {
        versions.push_back(str);
      }
    }

    if( versions.empty() ) {
      std::cerr << "Error in <main>: no corrections file to be replaced." << std::endl;
      exit(EXIT_FAILURE);
    }
    int versionId = versions.size()-1;

    // make copy
    std::string copyName = folder+"corrections"+std::to_string(id)+"-v"+std::to_string(versionId)+".json";
    std::ifstream latestFile(outName, std::ios_base::in);
    std::ofstream copyFile(copyName, std::ios_base::out);
    while( std::getline(latestFile, str) ){
      std::istringstream iss(str);
      copyFile << str << std::endl;
    }
    copyFile.close();
    latestFile.close();

    // create new file
    ConfigFileParser parser(copyName);

    // reset the stream to the beginning. We want to keep the 
    // already existing lines in the latest file, and update the 
    // corrections accordingly.
    std::ifstream refFile(copyName, std::ios_base::in);

    std::ofstream outFile(outName, std::ios_base::out);
    std::string planeName = "";
    TVector2 corr(0.0, 0.0);
    while( std::getline(refFile, str) ) {
      std::istringstream iss(str);
      // special characters sequences
      if( str == "{" || str == "\t}," || str == "\t}" || str == "}" ) outFile << str << std::endl;
      else {
        if( str.find("correction") == std::string::npos ) { // plane
          planeName = ExtractFromJSONQuotes(str);
          outFile << str << std::endl;
        }
        else {
          if( planeName == "" ) {
            std::cerr << "Error in <main>: no plane name assigned.\n";
            exit(EXIT_FAILURE);
          }
          else {
            if( std::get<1>(correctionsMap[planeName]) == -1.0 ) { // no corrections
              outFile << str << std::endl;
            }
            else {
              TVector2 current = parser.GetConfigValueFromListWithATVector2(planeName, "correction");

              if(  (planeName.find("X") != std::string::npos) 
                  ||(planeName.find("_1") != std::string::npos)
                  ||(planeName.find("_2") != std::string::npos) ) { 
                corr.Set(std::get<0>(correctionsMap[planeName]), 0.0);
              }
              else corr.Set(0.0,std::get<0>((correctionsMap[planeName])));
              outFile << "\t\t\"correction\" : [" << corr.X()+current.X() << ", " << corr.Y()+current.Y() << "]\n";
            }
          }
        }
      }
    }

    refFile.close();
    outFile.close();

    }

  canvas->Draw();

  if( _display ) gMyRootApp->Run(true);

  return 0;
}
