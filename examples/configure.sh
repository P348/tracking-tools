#!/bin/bash

echo -e "\033[1;32mConfiguring...\033[0m"
export TRACKINGTOOLS_DIR=$(pwd)/../
export GENFIT_INCDIR=$(grep -o 'GENFIT_INCLUDE_DIR:PATH=.*' ../build/CMakeCache.txt | cut -f2- -d=)
export GENFIT_LIB=$(grep -o 'GENFIT_LIBRARY:FILEPATH=.*' ../build/CMakeCache.txt | cut -f2- -d=)
echo -e "\033[0;31mTracking Tools is at ${TRACKINGTOOLS_DIR}\033[0m"
echo -e "\033[0;31mGenFit include directory is at ${GENFIT_INCDIR}\033[0m"
echo -e "\033[0;31mGenFit library is at ${GENFIT_LIB}\033[0m"
