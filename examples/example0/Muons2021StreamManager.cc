#include "Muons2021StreamManager.hh"
#include "Muons2021Globals.hh"

// STD library
#include <iostream>
#include <stdlib.h>

// ROOT
#include "TTree.h"

Muons2021StreamManager::Muons2021StreamManager()
{
  fInputTree = nullptr;
  fOutputTree = nullptr;
  fInputList = nullptr;
  fClone = false;

  fBrTrueMom = nullptr;
  fBrRecoMom = nullptr;
  fBrChisqMom = nullptr;
  fBrPullX = nullptr;
  fBrPullY = nullptr;
}

Muons2021StreamManager::~Muons2021StreamManager()
{
  // ...
}

void Muons2021StreamManager::SetInputTreeParser(TTree* aTree)
{
  fInputTree = aTree;
  if (fInputTree) {
    fInputTree->SetBranchAddress("Mom", &Muons2021::Mom);

    fInputTree->SetBranchAddress("X", &Muons2021::X);
    fInputTree->SetBranchAddress("Y", &Muons2021::Y);
    fInputTree->SetBranchAddress("Z", &Muons2021::Z);
    fInputTree->SetBranchAddress("Res", &Muons2021::Res);

    fInputList = fInputTree->GetUserInfo();
  }
  else {
    std::cerr << "No input <TTree> assigned." << std::endl;
    exit(EXIT_FAILURE);
  }
}

void Muons2021StreamManager::SetOutputTreeParser(TTree* aTree)
{
  fOutputTree = aTree;
  if (fOutputTree) {
    if (fClone) {
      fBrTrueMom = fOutputTree->Branch("TrueMom",   &Muons2021::TrueMom);
      fBrRecoMom = fOutputTree->Branch("RecoMom",  &Muons2021::RecoMom);
      fBrChisqMom = fOutputTree->Branch("ChisqMom", &Muons2021::ChisqMom);
      fBrPullX = fOutputTree->Branch("PullX", &Muons2021::PullX);
      fBrPullY = fOutputTree->Branch("PullY", &Muons2021::PullY);
    }
    else {
      fOutputTree->Branch("TrueMom",   &Muons2021::TrueMom);
      fOutputTree->Branch("RecoMom",  &Muons2021::RecoMom);
      fOutputTree->Branch("ChisqMom", &Muons2021::ChisqMom);
      fOutputTree->Branch("PullX", &Muons2021::PullX);
      fOutputTree->Branch("PullY", &Muons2021::PullY);
    }
  }
  else {
    std::cerr << "No output <TTree> assigned." << std::endl;
    exit(EXIT_FAILURE);
  }
}

void Muons2021StreamManager::ClearOutputTreeParser()
{
  if (fOutputTree) {
    Muons2021::TrueMom = -1.;
    Muons2021::RecoMom.clear();
    Muons2021::ChisqMom.clear();
    Muons2021::PullX.clear();
    Muons2021::PullY.clear();
  }
  else {
    std::cerr << "No output <TTree> assigned." << std::endl;
    exit(EXIT_FAILURE);
  }
}

void Muons2021StreamManager::SetCloneInputTree(bool Clone)
{
  fClone = Clone;
}

void Muons2021StreamManager::FillEvent()
{
  fOutputTree->Fill();
}
