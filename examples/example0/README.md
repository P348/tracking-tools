# example0

## Building the example
To build the example, pass following commands on the command line:
```sh
$ source build.sh
$ make -j4
```

## Running the example
The example is ran through:
```sh
$ ./main config.json
```

## Parsing the configuration file
The extension use for the configuration file is `.json` for simplicity to be used for the Boost library class `boost::property_tree`. <br/>
The typical variables used as input for the configuration are the following:

| Variable name | Description                                                                      | C++ type      |
| ------------- | -------------------------------------------------------------------------------- | ------------- |
| *algorithm*   | tracking algorithm to be used, possible inputs: *genfit*, *circle*, *analytical* | (std::string) |
| *pid*         | the particle id following GEANT4 PDG nomenclature                                | (int)         |
| *energy*      | the beam nominal energy [GeV]                                                    | (double)      |
| *smearing*    | the tracker resolution [mm]                                                      | (double)      |
| *planes*      | the number of tracking planes for the *analytical* algorithm, *n=3,4*            | (int)         |
| *gdml*        | the input .gdml file to be used with GenFit *genfit* algorithm                   | (std::string) |
| *input*       | the input .root file                                                             | (std::string) |
| *output*      | the output .root file                                                            | (std::string) |
| *clonetree*   | clone the input TTree into the output TTree, *n=0,1*                             | (int)         |
