#include "Muons2021Globals.hh"
#include "Muons2021StreamManager.hh"
#include "DetectorFactory.hh"
#include "TrackingManager.hh"
#include "MagnetMBPL.hh"
#include "TrackingGenFit.hh"
#include "TrackingCircle.hh"
#include "TrackingAnalytical.hh"
#include "ConfigFileParser.hh"

// STD Library
#include <iostream>
#include <sys/time.h>
#include <sys/resource.h>

// ROOT
#include "TFile.h"
#include "TTree.h"
#include "TClonesArray.h"

int main(int argc, const char* argv[])
{
  if (argc < 2) {
    std::cerr << "ERROR: Not enough arguments provided, enter:\n"
      << "Configuration file\n";
    exit(EXIT_FAILURE);
  }

  // parser
  ConfigFileParser* parser = new ConfigFileParser();
  parser->SetConfigFile(argv[1]);
  parser->PrintConfigFile();

  // input
  TFile* inFile = TFile::Open((parser->GetConfigValueWithAString("input")).c_str(), "read");
  TTree* inTree = (TTree*)inFile->Get("hits");

  // output
  TFile* outFile = new TFile((parser->GetConfigValueWithAString("output")).c_str(), "recreate");
  TTree* outTree;
  if (parser->GetConfigValueWithAnInt("clonetree") == 1) {
    outTree = inTree->CloneTree(0);
  }
  else {
    outTree = new TTree("reco", "reco");
  }

  Muons2021StreamManager* ioMgr = new Muons2021StreamManager();
  ioMgr->SetCloneInputTree(parser->GetConfigValueWithAnInt("clonetree"));
  ioMgr->SetInputTreeParser(inTree);
  ioMgr->SetOutputTreeParser(outTree);

  // define the tracking manager
  TrackingManager* trackingMgr = new TrackingManager();

  // init the factory
  DetectorFactory* factory = new DetectorFactory();
  factory->SetFactoryName("factory");
  factory->SetGDMLFile(parser->GetConfigValueWithAString("gdml"), 0);

  // define the magnet
  Magnet::MBPL* magnet = new Magnet::MBPL("MS2");
  Magnet::MBPLDimensions dim(-250.,250.,-100.,100.,3650.,5650.);
  magnet->SetMBPLDimensions(dim);
  magnet->SetFieldMag(1.4);
  magnet->SetFieldDir(TVector3(0.,-1.,0.));
  std::vector<Magnet::Generic*> magnets = {magnet};
  factory->RegisterNewMagnet(magnet->GetMagnetName(), magnet);
  trackingMgr->RegisterFactory(factory);

  // define the tracking algorithm
  TrackingGenFit* genfit = new TrackingGenFit();

  genfit->SetPDG(parser->GetConfigValueWithAnInt("pid"));
  genfit->SetNominalEnergy(parser->GetConfigValueWithADouble("energy"));
  genfit->SetGDMLManager(factory->GetGDMLGeoManager());
  genfit->SetMBPLField(true);
  genfit->SetNIter(8);
  genfit->InitFitter(TrackingGenFit::KalmanRefTrack);
  genfit->SetSmearing(parser->GetConfigValueWithADouble("smearing"));
  genfit->SetMagnets(magnets);
  genfit->InitGeomAndFields();
  genfit->InitMeasurementsFactory();

  trackingMgr->SetTrackingAlgorithm(genfit);

  // loop over the entries
  unsigned int n = inTree->GetEntries()/1.;
  for (unsigned int i = 0; i < n; i++) {
    inTree->GetEntry(i);

    // memory usage
    struct rusage r_usage;
    getrusage(RUSAGE_SELF, &r_usage);

    if (i%1000 == 0) {
      std::cout << "Processing event \033[1;36m" << i << "\033[0m..."  
        << " (check: maxrss = " << r_usage.ru_maxrss << " kb) ..." << std::endl;
    }

    // prepare the hits candidates
    std::vector<std::pair<TVector3, double> > Candidates;
    for (unsigned int k = 0; k < Muons2021::X->size(); k++) {
      TVector3 pos(Muons2021::X->at(k), Muons2021::Y->at(k), Muons2021::Z->at(k));
      Candidates.push_back(std::make_pair(pos, Muons2021::Res->at(k)));
    }

    trackingMgr->SetInputs(Candidates);
    trackingMgr->FindTrackCandidates();

    //trackingMgr->GetFactory()->PrintListOfRegisteredTrackers(true);
    trackingMgr->FitTrackCandidates();

    // pull of positions
    std::vector<Tracking::Track> tracks = genfit->GetReconstructedTracks();
    for (auto iter = tracks.begin(); iter != tracks.end(); iter++)
    {

      Tracking::Track& aTrack = *iter;
      if (aTrack.kTrueHits.size() == aTrack.kRecoHits.size()) {
        for (unsigned int k = 0; k < aTrack.kTrueHits.size(); k++) {
          TVector3 pos1 = aTrack.kTrueHits.at(k);
          TVector3 pos2 = aTrack.kRecoHits.at(k);
          Muons2021::PullX.push_back(pos1.X()-pos2.X());
          Muons2021::PullY.push_back(pos1.Y()-pos2.Y());
        }
      }

    }
    
    // fill the tree
    Muons2021::TrueMom = Muons2021::Mom;
    std::vector<std::pair<double, double> > fittedTracks = trackingMgr->GetFittedTracks();
    for (size_t k = 0; k < fittedTracks.size(); k++) {
      Muons2021::RecoMom.push_back(std::get<0>(fittedTracks.at(k)));
      Muons2021::ChisqMom.push_back(std::get<1>(fittedTracks.at(k)));
    }
    ioMgr->FillEvent();

    trackingMgr->Clean();
    ioMgr->ClearOutputTreeParser();

  }

  // write and close I/O
  inFile->Close();
  outTree->Write();
  outFile->Close();

  std::cout << "finished" << std::endl;

  // clean
  delete trackingMgr;
  delete ioMgr;
  delete parser;

}
