// User
#include "Muons2021Globals.hh"

namespace Muons2021{

  // input TTree
  double Mom;

  std::vector<double>* X = nullptr;
  std::vector<double>* Y = nullptr;
  std::vector<double>* Z = nullptr;
  std::vector<double>* Res = nullptr;

  // output TTree
  double TrueMom;
  std::vector<double> RecoMom;
  std::vector<double> ChisqMom;

  std::vector<double> PullX;
  std::vector<double> PullY;

} // end namespace Muons2021
