#ifndef Muons2021Globals_hh
#define Muons2021Globals_hh

// STD library
#include <vector>

namespace Muons2021{

  // input TTree
  extern double Mom;

  extern std::vector<double>* X;
  extern std::vector<double>* Y;
  extern std::vector<double>* Z;
  extern std::vector<double>* Res;

  // output TTree
  extern double TrueMom;
  extern std::vector<double> RecoMom;
  extern std::vector<double> ChisqMom;

  extern std::vector<double> PullX;
  extern std::vector<double> PullY;

} // end namespace Muons2021

#endif // end Muons2021Globals_hh
