#ifndef Muons2021StreamManager_hh
#define Muons2021StreamManager_hh

#include "IOStreamManager.hh"

// ROOT
#include "TTree.h"
#include "TList.h"
#include "TBranch.h"

class Muons2021StreamManager : public IOStreamManager {

  public:
    Muons2021StreamManager();
    ~Muons2021StreamManager();

    // TTree
    void SetInputTreeParser(TTree*);
    void SetOutputTreeParser(TTree*);

    void ClearOutputTreeParser();

    void SetCloneInputTree(bool);

    void FillEvent();

  private:

    bool fClone;

    TBranch* fBrTrueMom;
    TBranch* fBrRecoMom;
    TBranch* fBrChisqMom;
    TBranch* fBrPullX;
    TBranch* fBrPullY;

};


#endif // Muons2021StreamManager_hh
